/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to libavcodec major version. */
#define FF_AVCODEC_V "61"

/* Define to libavfilter major version. */
#define FF_AVFILTER_V "10"

/* Define to libavformat major version. */
#define FF_AVFORMAT_V "61"

/* Define to libavutil major version. */
#define FF_AVUTIL_V "59"

/* Define to libswscale major version. */
#define FF_POSTPROC_V "58"

/* Define to libswresample major version. */
#define FF_SWRESAMPLE_V "5"

/* Define to libswscale major version. */
#define FF_SWSCALE_V "8"

/* Define to 1 when xml output should be possible. */
#define HAVE_BG_XML 1

/* Define to 1 if FFmpeg should be loaded dynamically. */
#define HAVE_FF_DYNLOAD 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have libproc. */
/* #undef HAVE_LIBPROC */

/* Define to 1 if you have pthreads. */
#define HAVE_PTHREAD 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdio.h> header file. */
#define HAVE_STDIO_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Name of package */
#define PACKAGE "bs1770gain"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "info@pbelkner.de"

/* Define to the full name of this package. */
#define PACKAGE_NAME "bs1770gain"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "bs1770gain 0.9.5"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "bs1770gain"

/* Define to the home page for this package. */
#define PACKAGE_URL "http://pbelkner.de/"

/* Define to the version of this package. */
#define PACKAGE_VERSION "0.9.5"

/* Define to 1 if all of the C90 standard headers exist (not just the ones
   required in a freestanding environment). This macro is provided for
   backward compatibility; new code need not use it. */
#define STDC_HEADERS 1

/* Version number of package */
#define VERSION "0.9.5"
