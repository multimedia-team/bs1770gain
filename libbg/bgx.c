/*
 * bgx.c
 *
 * Copyright (C) 2014-2024 Peter Belkner <info@pbelkner.de>
 * Nanos gigantum humeris insidentes #TeamWhite
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.0 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */
#if defined (_WIN32) // [
#include <time.h>
#include <fcntl.h>
#include <getoptW.h>
#else // ] [
#include <ctype.h>
#include <getopt.h>
#if defined (__linux__) && defined (__GNUC__) // [
#include <gnu/libc-version.h>
#endif // ]
#endif // ]
#include <locale.h>
#include <bg.h>
#if defined (HAVE_FF_DYNLOAD) // [
#include "bg_version.h"
#endif // ]

///////////////////////////////////////////////////////////////////////////////
/*
 * once meant as an example program on how to deal with "libbg.a" (and in
 * turn with "libff.a") finally serving as bs1770gain's implementation.
 */
#if 1 // [
#if 1 // [
#define BG_TEAM_WHITE \
    " #WLM"
#else // ] [
#define BG_TEAM_WHITE \
    ""
#endif // ]
#define BG_NANOS_GIGANTUM_HUMERIS_INSIDENTES \
    "Nanos gigantum humeris insidentes." BG_TEAM_WHITE "\n"
#endif // ]

///////////////////////////////////////////////////////////////////////////////
#if defined (HAVE_BG_XML) // [
	void bg_set_output_xml(bg_param_t *param)
	{
  	param->print.vmt=&bg_print_xml_vmt;
	}

	int bg_is_output_xml(const bg_param_t *param)
	{
  	return param->print.vmt==&bg_print_xml_vmt;
	}
#else // ] [
	void bg_set_output_xml(bg_param_t *param FFUNUSED)
	{
	}

	int bg_is_output_xml(const bg_param_t *param FFUNUSED)
	{
  	return 0;
	}
#endif // ]

///////////////////////////////////////////////////////////////////////////////
static const ffchar_t *bg_version(const ffchar_t *path, FILE *f)
{
  path=FFBASENAME(path);

  _FPRINTFV(f,"%" PBU_PRIs,path);
#if defined (PACKAGE_VERSION) // [
  _FPRINTFV(f," %s",PACKAGE_VERSION);
#endif // ]
  _FPRINTF(f,", Copyright (C) Peter Belkner 2014-2024.\n"),
#if defined (BG_NANOS_GIGANTUM_HUMERIS_INSIDENTES) // [
  _FPRINTFV(f,"%s",BG_NANOS_GIGANTUM_HUMERIS_INSIDENTES);
#endif // ]
#if defined (PACKAGE_URL) // [
  _FPRINTFV(f,"%s\n",PACKAGE_URL);
#endif // ]

#if defined (_WIN32) // [
#if defined (_WIN64) // [
  _FPRINTFV(f,"%s","Compiled for Windows 64 bit");
#else // ] [
  _FPRINTFV(f,"%s","Compiled for Windows 32 bit");
#endif // ]
#elif defined (HAVE_FF_DYNLOAD) && defined (BG_POSIX_SYSNAME) // ] [
  _FPRINTFV(f,"Compiled for %s",BG_POSIX_SYSNAME);
#else // ] [
  _FPRINTFV(f,"%s","Compiled");
#endif // ]
#if defined (__GNUC__) // [
  _FPRINTFV(f," by means of gcc %d.%d.%d",__GNUC__,__GNUC_MINOR__,
      __GNUC_PATCHLEVEL__);
#endif // ]
#if defined (BG_WINDOWS_MAJOR) // [
#if defined (BG_WINDOWS_CSD_VESIONA) // [
  if (BG_WINDOWS_CSD_VESIONA[0]) {
    _FPRINTFV(f," on Windows %d.%d.%d\n(%s) expecting\n",
        BG_WINDOWS_MAJOR,
        BG_WINDOWS_MINOR,
        BG_WINDOWS_BUILD_NUMBER,
        BG_WINDOWS_CSD_VESIONA);
  }
  else {
#endif // ]
    _FPRINTFV(f," on Windows %d.%d.%d\nexpecting\n",
        BG_WINDOWS_MAJOR,
        BG_WINDOWS_MINOR,
        BG_WINDOWS_BUILD_NUMBER);
#if defined (BG_WINDOWS_CSD_VESIONA) // [
  }
#endif // ]
#elif defined (HAVE_FF_DYNLOAD) // ] [
  _FPRINTF(f," on\n");

  if (BG_POSIX_NODENAME[0])
    _FPRINTFV(f,"       nodename:  %s,\n",BG_POSIX_NODENAME);

  if (BG_POSIX_RELEASE[0])
    _FPRINTFV(f,"        release:  %s,\n",BG_POSIX_RELEASE);

  if (BG_POSIX_VERSION[0])
    _FPRINTFV(f,"        version:  %s,\n",BG_POSIX_VERSION);

  if (BG_POSIX_MACHINE[0])
    _FPRINTFV(f,"        machine:  %s,\n",BG_POSIX_MACHINE);

#if defined (BG_POSIX_DOMAINNAME) // [
  if (BG_POSIX_DOMAINNAME[0])
    _FPRINTFV(f,"     domainname:  %s,\n",BG_POSIX_DOMAINNAME);
#endif // ]

  _FPRINTF(f,"expecting\n");
#else // ] [
  _FPRINTF(f," expecting\n");
#endif // ]

#if defined (HAVE_FF_DYNLOAD) && defined (BG_GNU_LIBC_VERSION) // [
  _FPRINTFV(f,"           libc:  %s (%s),\n",
      BG_GNU_LIBC_VERSION,BG_GNU_LIBC_RELEASE);
#endif // ]
  _FPRINTFV(f,"      libavutil:  %d.%d.%d,\n",
      LIBAVUTIL_VERSION_MAJOR,
      LIBAVUTIL_VERSION_MINOR,
      LIBAVUTIL_VERSION_MICRO);
  _FPRINTFV(f,"  libswresample:  %d.%d.%d,\n",
      LIBSWRESAMPLE_VERSION_MAJOR,
      LIBSWRESAMPLE_VERSION_MINOR,
      LIBSWRESAMPLE_VERSION_MICRO);
  _FPRINTFV(f,"     libavcodec:  %d.%d.%d,\n",
      LIBAVCODEC_VERSION_MAJOR,
      LIBAVCODEC_VERSION_MINOR,
      LIBAVCODEC_VERSION_MICRO);
  _FPRINTFV(f,"    libavformat:  %d.%d.%d,\n",
      LIBAVFORMAT_VERSION_MAJOR,
      LIBAVFORMAT_VERSION_MINOR,
      LIBAVFORMAT_VERSION_MICRO);
  _FPRINTFV(f,"     libswscale:  %d.%d.%d,\n",
      LIBSWSCALE_VERSION_MAJOR,
      LIBSWSCALE_VERSION_MINOR,
      LIBSWSCALE_VERSION_MICRO);
  _FPRINTFV(f,"    libpostproc:  %d.%d.%d, and\n",
      LIBPOSTPROC_VERSION_MAJOR,
      LIBPOSTPROC_VERSION_MINOR,
      LIBPOSTPROC_VERSION_MICRO);
  _FPRINTFV(f,"    libavfilter:  %d.%d.%d.\n",
      LIBAVFILTER_VERSION_MAJOR,
      LIBAVFILTER_VERSION_MINOR,
      LIBAVFILTER_VERSION_MICRO);
  _FPRINTF(f,"This is free software; see the source for copying conditions."
      "  There is NO\n"
      "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR"
      " PURPOSE.\n");
  return path;
}

#if defined (BG_SAMPLES_COUNT) // [
//#define UPSAMPLER_THRESHOULD
#endif // ]
static void bg_usage(const ffchar_t *path, FILE *f)
{
  path=bg_version(path,f);

  _FPRINTFV(f,"\nUsage: %" PBU_PRIs,path);

  _FPRINTF(f," [options] <file/dir> [<file/dir> ...]\n\n");
  _FPRINTF(f,"Options:\n");
  _FPRINTF(f," -v,--version:  print this message and exit\n");
  _FPRINTF(f," -h,--help[=<topic>]?:  print this message and exit\n"
      "    (optional argument <topic> out of \"codec\" or \"suffix\")\n");
  /////////////////////////////////////////////////////////////////////////////
  _FPRINTF(f," -i,--integrated:  calculate integrated loudness\n");
  _FPRINTF(f," -s,--shortterm:  calculate maximum shortterm loudness\n");
  _FPRINTF(f," -m,--momentary:  calculate maximum momentary loudness\n");
#if defined (BG_LOUDNESS_ALL) // [
  _FPRINTF(f," --loudness-all:  the same as -ism --shortterm-mean\n");
#endif // ]
  _FPRINTF(f," -r,--range:  calculate loudness range\n");
  _FPRINTF(f," -p,--sample-peak:  calculate maximum sample peak\n");
#if defined (BG_SAMPLES_COUNT) // [
#if defined (UPSAMPLER_THRESHOULD) // [
  _FPRINTF(f," -t,--true-peak:  calculate maximum true peak\n");
#else // ] [
  _FPRINTF(f," -t,--true-peak[=<number of samples to skip>]?:  calculate\n"
      "   maximum true peak after having skipped the specified number\n"
      "   of samples from the up-sampler (default=0)\n");
#endif // ]
#endif // ]
  _FPRINTF(f," -b <timestamp>,--begin=<timestamp>:  begin decoding at\n"
      "   timestamp (in microseconds, format: hh:mm:ss.ms)\n");
  _FPRINTF(f," -d <duration>,--duration=<duration>:  let decoding\n"
      "   last duration (in microseconds, format: hh:mm:ss.ms)\n");
  _FPRINTF(f," -u <method>,--use=<method>:  base replaygain calculation on\n"
      "   <method> (with respect to the -a/--apply and -o/--output\n"
      "   options. available methods:\n"
      "     integrated (default),\n"
      "     momentary, or\n"
      "     shortterm\n"
      "   experimental methods:\n"
      "     momentary-mean (synonym for \"integrated\"),\n"
      "     momentary-maximum (synonym for \"momentary\"),\n"
      "     shortterm-mean, or\n"
      "     shortterm-maximum (synonym for \"shortterm\")\n");
  _FPRINTF(f," -a [<weight>]/--apply[=<weight>]:  apply the EBU/ATSC/RG\n"
      "   album gain (in conjunction with the -o/--output option.)\n"
      "   when <weight> out of [0.0 .. 1.0] is provided: album gain\n"
      "   plus <weight>*(track gain - album gain).\n");
  _FPRINTF(f," -o <folder>,--output=<folder>:  write replaygain tags\n"
      "   or apply the EBU/ATSC/RG gain, respectively,\n"
      "   and output to <folder>.\n"
      "   specify either option -o/--output or option --overwrite\n"
      "   but not both.\n");
#if defined (BG_PARAM_PREAMP) // [
  _FPRINTF(f," --preamp=<ffloat>\n");
#endif // ]
#if ! defined (BG_UNICODE) // [
  _FPRINTF(f," -f <file>,--file=<file>:  write analysis to an utf-8 log"
      " file\n");
  _FPRINTF(f," --utf-16[=<file>]:  write an utf-16 log file instead of an"
      " utf-8\n"
      "   log file (<file> can be omitted when provided by means of the\n"
      "   -f/--file option.)\n");
#else // ] [
  _FPRINTF(f," -f <file>,--file=<file>:  write analysis to a log file\n");
#endif // ]
#if defined (BG_PARAM_REFERENCE) // [
  _FPRINTF(f," --reference=<path to a refernce tree>:  path to a tree\n"
      "   structural equivalent to the input tree and its's leafes may\n"
      "   serve as a reference to it's correponding input leafes.\n");
#endif // ]
#if defined (BG_PARAM_SKIP_SCAN) // [
  _FPRINTF(f," --skip-scan:  skip counting files.\n");
#endif // ]
#if defined (BG_SAMPLES_COUNT) && defined (UPSAMPLER_THRESHOULD) // [
  _FPRINTF(f," --upsampler-threshould=<number of samples>:  number of\n"
      "   samples to skip when upsampling for true peak calculation\n");
#endif // ]
#if defined (BG_PARAM_SCRIPT) // [
  _FPRINTF(f," --script=<script>:  run <script> on track. if not explicitely\n"
      "   provided implies option --apply. <script> might be provided\n"
      "   as a string or as a file name.\n");
#endif // ]
#if defined (BG_PARAM_SHELL) // [
  _FPRINTF(f," --shell=<path to interpreter>:  interpret <script> according\n"
      "   to interpreter.\n");
#endif // ]
#if defined (BG_PARAM_QUIET) // [
  _FPRINTF(f," --quiet:  supress any output except error messages.\n");
#endif // ]
#if defined (BG_PARAM_THREADS) // [
  _FPRINTF(f," --threads=<number of threads>:  parallelize using <number\n"
      "     of threads> threads. implies --quiet.\n");
#endif // ]
  /////////////////////////////////////////////////////////////////////////////
  _FPRINTF(f," --ebu:  calculate replay gain according to EBU R128\n"
      "   (-23.0 LUFS, default)\n");
  _FPRINTF(f," --atsc:  calculate replay gain according to ATSC A/85\n"
      "   (-24.0 LUFS)\n");
  _FPRINTF(f," --replaygain:  calculate replay gain according to\n"
      "   ReplayGain 2.0 (-18.0 LUFS)\n");
  _FPRINTF(f," --track-tags:  write track tags\n");
  _FPRINTF(f," --album-tags:  write album tags\n");
  _FPRINTF(f," --tag-prefix=<prefix>:  instead of \"REPLAYGAIN\",\n"
      "   use <prefix> as replaygain tag prefix\n");
  _FPRINTF(f," --unit=<unit>:  write results and tags with <unit> out of\n"
      "   \"ebu\" or \"db\"\n");
#if defined (FF_SERGEY_INDEX_BUGFIX) // [
  _FPRINTF(f," --audio=<index>/--ai=<index>:  select audio index"
      " (corresponds\n"
      "   to [0:<index>] in FFmpeg listing, cf. -l/--list[=out]? option\n");
  _FPRINTF(f," --video=<index>/--vi=<index>:  select video index"
      " (corresponds\n"
      "   to [0:<index>] in FFmpeg listing, cf. -l/--list[=out]? option\n");
#endif // ]
#if defined (FF_INPUT_LIST) // [
  _FPRINTF(f," --l/--list[=out]?  display the file's FFmpeg listing(s).\n");
#endif // ]
  _FPRINTF(f," --matrix:<matrix>:  remix to <matrix> out of\n"
      "    front-left,\n"
      "    front-right,\n"
      "    front-center,\n"
      "    low-frequency,\n"
      "    back-left,\n"
      "    back-right,\n"
      "    front-left-of-center,\n"
      "    front-right-of-center,\n"
      "    back-center,\n"
      "    side-left,\n"
      "    side-right,\n"
      "    top-center,\n"
      "    top-front-left,\n"
      "    top-front-center,\n"
      "    top-front-right,\n"
      "    top-back-left,\n"
      "    top-back-center,\n"
      "    top-back-right,\n"
      "    stereo-left,\n"
      "    stereo-right,\n"
      "    wide-left,\n"
      "    wide-right,\n"
      "    surround-direct-left,\n"
      "    surround-direct-right,\n"
      "    low-frequency-2,\n"
      "    layout-mono,\n"
      "    layout-stereo,\n"
      "    layout-2point1,\n"
      "    layout-2-1,\n"
      "    layout-surround,\n"
      "    layout-3point1,\n"
      "    layout-4point0,\n"
      "    layout-4point1,\n"
      "    layout-2-2,\n"
      "    layout-quad,\n"
      "    layout-5point0,\n"
      "    layout-5point1,\n"
      "    layout-5point0-back,\n"
      "    layout-5point1-back,\n"
      "    layout-6point0,\n"
      "    layout-6point0-front,\n"
      "    layout-hexagonal,\n"
      "    layout-6point1,\n"
      "    layout-6point1-back,\n"
      "    layout-6point1-front,\n"
      "    layout-7point0,\n"
      "    layout-7point0-front,\n"
      "    layout-7point1,\n"
      "    layout-7point1-wide,\n"
      "    layout-7point1-wide-back,\n"
      "    layout-octagonal,\n"
      "    layout-hexadecagonal, or\n"
      "    layout-stereo-downmix\n");
  _FPRINTF(f," --stereo:  shorthand for --matrix=layout-stereo\n");
  _FPRINTF(f," --drc=<float>:  set AC3 dynamic range compression (DRC)\n");
  _FPRINTF(f," -x [<extension>]?, --extension[=<extension>]?:  enable"
      " extension\n"
      "   out of\n"
      "    rename:  rename files according to TITLE tag\n"
      "    csv:  read metadata from per-folder CSV file \"folder.csv\"\n"
      "    copy:  copy non-audio files from source to destination\n"
      "      folder\n"
      "    tags:  automatically add the TRACK and DISC tags\n"
      "    all:  all of the above (can be omitted)\n");
  _FPRINTF(f," --suffix=<suffix>:  output to <basename>.<suffix>\n"
      "    (only useful in conjunction with option -o/--output)\n");
  _FPRINTF(f," --loglevel=<level>:  set FFmpeg loglevel to <level> out of\n"
      "    quiet,\n"
      "    panic,\n"
      "    fatal,\n"
      "    error,\n"
      "    warning,\n"
      "    info,\n"
      "    verbose,\n"
      "    debug, or\n"
      "    trace\n");
#if defined (HAVE_BG_XML) // [
#if defined (BG_PARAM_XML_CDATA) // [
  _FPRINTF(f," --xml[=cdata]?:  print results in xml format. with the\n"
      "    optional argument cdata given, use a modified schema\n"
      "    (incompatible with prior versions) aimed to correctly\n"
      "    represent file names by means of a respective CDATA\n"
      "    section.\n");
#else // ] [
  _FPRINTF(f," --xml:  print results in xml format\n");
#endif // ]
#endif // ]
  _FPRINTF(f," --csv[=<separator character>]?:  print results in csv\n"
      "    format using the specified separator character (defaults\n"
      "    to tabulator character)\n");
#if defined (BG_CLOCK) // [
  _FPRINTF(f," --time:  print out duration of program invocation\n");
#endif // ]
  _FPRINTF(f," --norm=<float>:  norm loudness to float.\n");
#if 0 // [
  _FPRINTF(f," --preamp=<preamp>:\n");
  _FPRINTF(f," --stero:\n");
  _FPRINTF(f," --rg-tags:\n");
  _FPRINTF(f," --bwf-tags:\n");
#endif // ]
  _FPRINTF(f," --overwrite:  replace any source file by its respective\n"
      "    target file. specify either option -o/--output or option\n"
      "    --overwrite but not both.\n"
      "    WARNING:  use this option with extrem care. The source\n"
      "      files will definitely be lost!!! For this reason using\n"
      "      this option is discouraged. It's provided just for\n"
      "      completeness.\n");
#if defined (BG_PARAM_LFE) && defined (BG_CHANNEL_LFE) // [
  _FPRINTFV(f," --lfe=<lfe channel>  overwrite the default <lfe"
      " channel>\n"
      "    (default: %d.)\n",BG_CHANNEL_LFE);
#endif // ]
  _FPRINTF(f," --codec=<name>:  use audio codec \"<name>\" for output\n"
      "    default: \"flac\".)\n");
  _FPRINTF(f," --temp-prefix=<prefix>:  create temporary files with\n"
      "    prefix \"<prefix>\" default: \"" BG_TEMP_PREFIX "\".)\n");
  _FPRINTF(f," --suppress-hierarchy:  suppress printing results up the\n"
      "    hierarchy.\n");
  _FPRINTF(f," --suppress-progress:  suppress printing processing"
      " progress\n");
#if defined (BG_WARN_CHANNEL_OVERFLOW_DISABLE) // [
  _FPRINTF(f," --disable_warn_channel_overflow\n");
#endif // ]
#if defined (BG_OPPOSITE) // [
  _FPRINTF(f," --opposite-sign,--opposite:  print absolute values with\n"
         "   the opposite sign compared to the traditional approach\n"
	 "   (correcting a long standing bug)\n");
#endif // ]
  /////////////////////////////////////////////////////////////////////////////
  _FPRINTF(f,"\nExperimental options:\n");
  ////////
  _FPRINTF(f,"1) momentary block\n");
  _FPRINTF(f," --momentary-mean:  calculate mean loudness based on\n"
      "   momentary block (same as --integrated)\n");
  _FPRINTF(f," --momentary-maximum:  calculate maximum loudness based\n"
      "   on momentary block (same as --momentary)\n");
  _FPRINTF(f," --momentary-range:  calculate loudness range based on\n"
      "   momentary block\n");
  _FPRINTF(f," --momentary-length=<ms>:  length of momentary block\n"
      "   in milliseconds (default: 400)\n");
  _FPRINTF(f," --momentary-overlap=<percent>:  overlap of momentary\n"
      "   block in percent (default: 75)\n");
  _FPRINTF(f," --momentary-mean-gate=<gate>:  silence gate for mean\n"
      "   measurement of momentary block (default: -10.0)\n");
  _FPRINTF(f," --momentary-range-gate=<gate>:  silence gate for range\n"
      "   measurement of momentary block (default: -20.0)\n");
  _FPRINTF(f," --momentary-range-lower-bound=<float>:  lower bound for\n"
      "   range measurement of momentary block (default: 0.1)\n");
  _FPRINTF(f," --momentary-range-upper-bound=<float>:  upper bound for\n"
      "   range measurement of momentary block (default: 0.95)\n");
  ////////
  _FPRINTF(f,"2) shortterm block\n");
  _FPRINTF(f," --shortterm-mean:  calculate mean loudness based on\n"
      "   shortterm block\n");
  _FPRINTF(f," --shortterm-maximum:  calculate maximum loudness based\n"
      "   on shortterm block (same as --shortterm)\n");
  _FPRINTF(f," --shortterm-range:  calculate loudness range based on\n"
      "   shortterm block (same as --range)\n");
  _FPRINTF(f," --shortterm-length=<ms>:  length of shortterm block\n"
      "   in milliseconds (default: 3000)\n");
  _FPRINTF(f," --shortterm-overlap <percent>:  overlap of shortterm\n"
      "   block in percent (default: 67)\n");
  _FPRINTF(f," --shortterm-mean-gate=<gate>:  silence gate for mean\n"
      "   measurement of shortterm block (default: -10.0)\n");
  _FPRINTF(f," --shortterm-range-gate=<gate>:  silence gate for range\n"
      "   measurement of shortterm block (default: -20.0)\n");
  _FPRINTF(f," --shortterm-range-lower-bound=<float>:  lower bound for\n"
      "   range measurement of shortterm block (default: 0.1)\n");
  _FPRINTF(f," --shortterm-range-upper-bound=<float>:  upper bound for\n"
      "   range measurement of shortterm block (default: 0.95)\n");
  /////////////////////////////////////////////////////////////////////////////
  _FPRINTF(f,"\n");
  _FPRINTF(f,"Command line arguments may appear in any order.\n");
#if ! defined (HAVE_BG_XML) // [
  _FPRINTF(f,"\n");
  _FPRINTF(f,"This instance of bs1770gain was configured without option\n"
      "   --enable-xml. Hence no option --xml. If you're interested in num-\n"
			"   bers try --cvs.\n");
#endif // ]
#if defined (BG_TRADITIONAL) // [
  _FPRINTF(f,"\n");
  _FPRINTF(f,"ATTENTION: Starting with version 0.9.3 bs1770gain"
	" corrects a bug\n"
	"           dating back to the very beginning."
        " It now issues the\n"
	"           correct values for the relative\n"
	"               * momentary mean\n"
	"               * momentary maximum\n"
	"               * shortterm mean\n"
	"               * shortterm maximum\n"
	"           loudness instead of the opposite. If you insist on the\n"
	"           errornous traditional approach run with long option\n"
	"               --traditional.\n");
#endif // ]
}

#if defined (HAVE_FF_DYNLOAD) && defined (__linux__) \
    && defined (__GNUC__) // [
///////////////////////////////////////////////////////////////////////////////
static int strcmpex(const char **s1, const char **s2, int ch)
{
  for (;;) {
    int lhs=ch==**s1?0:**s1;
    int rhs=ch==**s2?0:**s2;
    int cmp=lhs-rhs;

    if (cmp||!lhs||!rhs) {
      if (ch==**s1)
        ++*s1;

      if (ch==**s2)
        ++*s2;

      return cmp;
    }
    else {
      ++*s1;
      ++*s2;
    }
  }
}
#endif // ]

///////////////////////////////////////////////////////////////////////////////
// https://stackoverflow.com/questions/190543/how-can-i-change-the-width-of-a-windows-console-window
// https://docs.microsoft.com/en-us/windows/console/window-and-screen-buffer-size
// David Tran: Logging Function Entry and Exit in C
//  https://davidtranscend.com/blog/log-function-entry-exit-c/
#if defined (_WIN32) // [
int wmain(int argc, wchar_t *const *argv)
#else // ] [
int main(int argc, char *const *argv)
#endif // ]
{
  enum {
#if 0 // [
    BG_ARG_APPLY=FFL('z')+1,
    BG_ARG_UNIT,
#else // ] [
    BG_ARG_UNIT=FFL('z')+1,
#endif // ]
#if defined (BG_PARAM_PREAMP) // [
  	BG_PREAMP,
#endif // ]
#if defined (_WIN32) // [
    BG_ARG_UTF_16,
#endif // ]
#if defined (BG_PARAM_REFERENCE) // [
    BG_REFERENCE,
#endif // ]
#if defined (BG_PARAM_SKIP_SCAN) // [
    BG_SKIP_SCAN,
#endif // ]
#if defined (BG_PARAM_SCRIPT) // [
    BG_SCRIPT,
#endif // ]
#if defined (BG_SAMPLES_COUNT) && defined (UPSAMPLER_THRESHOULD) // [
    BG_UPSAMPLER_THRESHOULD,
#endif // ]
#if defined (BG_PARAM_SHELL) // [
    BG_SHELL,
#endif // ]
    BG_ARG_DRC,
    BG_ARG_LOGLEVEL,
    BG_ARG_NORM,
    BG_ARG_PREAMP,
#if defined (BG_CLOCK) // [
    BG_ARG_TIME,
#endif // ]
    BG_ARG_EBU,
    BG_ARG_SUFFIX,
    BG_ARG_ATSC,
    BG_ARG_AUDIO,
#if defined (FF_SERGEY_INDEX_BUGFIX) // [
    BG_ARG_VIDEO,
#endif // ]
    BG_ARG_MATRIX,
#if defined (FF_INPUT_LIST) // [
    BG_ARG_LIST,
#endif // ]
    BG_ARG_STEREO,
    BG_ARG_REPLAYGAIN,
    BG_ARG_RG_TAGS,
#if defined (BG_BWF_TAGS) // [
    BG_ARG_BWF_TAGS,
#endif // ]
    BG_ARG_TAGS_TRACK,
    BG_ARG_TAGS_ALBUM,
    BG_ARG_XML,
    BG_ARG_CSV,
    BG_ARG_TAG_PREFIX,
    BG_ARG_OVERWRITE,
#if defined (BG_PARAM_LFE) // [
    BG_ARG_CH_LFE,
#endif // ]
    BG_ARG_OUTPUT_CODEC,
    BG_ARG_TEMP_PREFIX,
    BG_ARG_SUPPRESS_HIERARCHY,
    BG_ARG_SUPPRESS_PROGRESS,
#if defined (BG_PARAM_QUIET) // [
    BG_ARG_QUIET,
#endif // ]
#if defined (BG_PARAM_THREADS) // [
    BG_ARG_NTHREADS,
#endif // ]
#if defined (BG_PARAM_SLEEP) // [
    BG_ARG_SLEEP,
#endif // ]
#if defined (BG_WARN_CHANNEL_OVERFLOW_DISABLE) // [
    BG_ARG_WARN_CHANNEL_OVERFLOW_DISABLE,
#endif // ]
    BG_ARG_EXTENSION,
    ////////////////
#if 0 // [
    BG_ARG_MOMENTARY_MEAN,
    BG_ARG_MOMENTARY_MAXIMUM,
#endif // ]
    BG_ARG_MOMENTARY_RANGE,
    BG_ARG_MOMENTARY_LENGTH,
    BG_ARG_MOMENTARY_OVERLAP,
    BG_ARG_MOMENTARY_MEAN_GATE,
    BG_ARG_MOMENTARY_RANGE_GATE,
    BG_ARG_MOMENTARY_RANGE_LOWER_BOUND,
    BG_ARG_MOMENTARY_RANGE_UPPER_BOUND,
    ////////////////
    BG_ARG_SHORTTERM_MEAN,
#if 0 // [
    BG_ARG_SHORTTERM_MAXIMUM,
    BG_ARG_SHORTTERM_RANGE,
#endif // ]
    BG_ARG_SHORTTERM_LENGTH,
    BG_ARG_SHORTTERM_OVERLAP,
    BG_ARG_SHORTTERM_MEAN_GATE,
    BG_ARG_SHORTTERM_RANGE_GATE,
    BG_ARG_SHORTTERM_RANGE_LOWER_BOUND,
    BG_ARG_SHORTTERM_RANGE_UPPER_BOUND,
#if defined (BG_TRADITIONAL) // [
    BG_ARG_TRADITIONAL,
#elif defined (BG_OPPOSITE) // [
    BG_ARG_OPPOSITE,
#endif // ]
#if defined (BG_LOUDNESS_ALL) // [
    BG_ARG_LOUDNESS_ALL,
#endif // ]
  };

  enum BGFlagHelpArg {
    BG_FLAG_HELP_ARG_NULL=1<<0,
    BG_FLAG_HELP_ARG_VERSION=1<<1,
    BG_FLAG_HELP_ARG_SUFFIX=1<<2,
    BG_FLAG_HELP_ARG_CODEC=1<<3,
  };

#if defined (BG_CLOCK) // [
  enum {
    BG_MIN=60,
    BG_HOUR=60*BG_MIN,
  };
#endif // ]

#if defined (HAVE_FF_DYNLOAD) // [
  static const ffchar_t TOOLS[]=FFL("bs1770gain-tools");
#endif // ]
  static const ffchar_t FLAGS[]=
      FFL("hvu:ptismrb:d:o:f:alx");

  static struct option OPTS[]={
    { FFL("version"),no_argument,NULL,FFL('v') },
    { FFL("help"),optional_argument,NULL,FFL('h') },
    { FFL("begin"),required_argument,NULL,FFL('b') },
    { FFL("duration"),required_argument,NULL,FFL('d') },
    { FFL("file"),required_argument,NULL,FFL('f') },
    { FFL("utf-8"),required_argument,NULL,FFL('f') },
    { FFL("output"),required_argument,NULL,FFL('o') },
    { FFL("use"),required_argument,NULL,FFL('u') },
    { FFL("integrated"),no_argument,NULL,FFL('i') },
    { FFL("momentary"),no_argument,NULL,FFL('m') },
    { FFL("range"),no_argument,NULL,FFL('r') },
    { FFL("shortterm"),no_argument,NULL,FFL('s') },
    { FFL("sample-peak"),no_argument,NULL,FFL('p') },
    { FFL("samplepeak"),no_argument,NULL,FFL('p') },
#if defined (BG_SAMPLES_COUNT) // [
#if defined (UPSAMPLER_THRESHOULD) // [
    { FFL("true-peak"),no_argument,NULL,FFL('t') },
    { FFL("truepeak"),no_argument,NULL,FFL('t') },
#else // ] [
    { FFL("true-peak"),optional_argument,NULL,FFL('t') },
    { FFL("truepeak"),optional_argument,NULL,FFL('t') },
#endif // ]
#endif // ]
    { FFL("apply"),optional_argument,NULL,FFL('a') },
#if defined (_WIN32) // [
    { FFL("utf-16"),optional_argument,NULL,BG_ARG_UTF_16 },
#endif // ]
#if defined (BG_PARAM_SKIP_SCAN) // [
    { FFL("skip-scan"),optional_argument,NULL,BG_SKIP_SCAN },
#endif // ]
#if defined (BG_PARAM_PREAMP) // [
    { FFL("preamp"),required_argument,NULL,BG_PREAMP },
#endif // ]
#if defined (BG_PARAM_REFERENCE) // [
    { FFL("reference"),required_argument,NULL,BG_REFERENCE },
#endif // ]
#if defined (BG_PARAM_SCRIPT) // [
    { FFL("script"),required_argument,NULL,BG_SCRIPT },
#endif // ]
#if defined (BG_SAMPLES_COUNT) && defined (UPSAMPLER_THRESHOULD) // [
    { FFL("upsampler-threshould"),required_argument,NULL,
        BG_UPSAMPLER_THRESHOULD },
#endif // ]
#if defined (BG_PARAM_SHELL) // [
    { FFL("shell"),required_argument,NULL,BG_SHELL },
#endif // ]
    { FFL("unit"),required_argument,NULL,BG_ARG_UNIT },
    { FFL("audio"),required_argument,NULL,BG_ARG_AUDIO },
    { FFL("ai"),required_argument,NULL,BG_ARG_AUDIO },
    { FFL("drc"),required_argument,NULL,BG_ARG_DRC },
#if defined (BG_PARAM_QUIET) // [
    { FFL("quiet"),no_argument,NULL,BG_ARG_QUIET },
#endif // ]
#if defined (BG_PARAM_THREADS) // [
    { FFL("threads"),required_argument,NULL,BG_ARG_NTHREADS },
#endif // ]
#if defined (BG_PARAM_SLEEP) // [
    { FFL("sleep"),required_argument,NULL,BG_ARG_SLEEP },
#endif // ]
    { FFL("extension"),optional_argument,NULL,BG_ARG_EXTENSION },
    { FFL("suffix"),required_argument,NULL,BG_ARG_SUFFIX },
    { FFL("loglevel"),required_argument,NULL,BG_ARG_LOGLEVEL },
    { FFL("norm"),required_argument,NULL,BG_ARG_NORM },
    { FFL("preamp"),required_argument,NULL,BG_ARG_PREAMP },
#if defined (FF_SERGEY_INDEX_BUGFIX) // [
    { FFL("video"),required_argument,NULL,BG_ARG_VIDEO },
    { FFL("vi"),required_argument,NULL,BG_ARG_VIDEO },
#endif // ]
    { FFL("time"),no_argument,NULL,BG_ARG_TIME },
    { FFL("ebu"),no_argument,NULL,BG_ARG_EBU },
    { FFL("atsc"),no_argument,NULL,BG_ARG_ATSC },
    { FFL("matrix"),required_argument,NULL,BG_ARG_MATRIX },
    { FFL("list"),optional_argument,NULL,'l' },
    { FFL("stereo"),no_argument,NULL,BG_ARG_STEREO },
    { FFL("replaygain"),no_argument,NULL,BG_ARG_REPLAYGAIN },
#if defined (BG_BWF_TAGS) // [
    { FFL("rg-tags"),no_argument,NULL,BG_ARG_TAGS_RG },
    { FFL("bwf-tags"),no_argument,NULL,BG_ARG_TAGS_BWF },
#endif // ]
    { FFL("track-tags"),no_argument,NULL,BG_ARG_TAGS_TRACK },
    { FFL("album-tags"),no_argument,NULL,BG_ARG_TAGS_ALBUM },
#if defined (HAVE_BG_XML) // [
#if defined (BG_PARAM_XML_CDATA) // [
    { FFL("xml"),optional_argument,NULL,BG_ARG_XML },
#else // ] [
    { FFL("xml"),no_argument,NULL,BG_ARG_XML },
#endif // ]
#endif // ]
    { FFL("csv"),optional_argument,NULL,BG_ARG_CSV },
    { FFL("tag-prefix"),required_argument,NULL,BG_ARG_TAG_PREFIX },
    { FFL("overwrite"),no_argument,NULL,BG_ARG_OVERWRITE },
#if defined (BG_PARAM_LFE) // [
    { FFL("lfe"),required_argument,NULL,BG_ARG_CH_LFE },
#endif // ]
    { FFL("codec"),required_argument,NULL,BG_ARG_OUTPUT_CODEC },
    { FFL("temp-prefix"),required_argument,NULL,BG_ARG_TEMP_PREFIX },
    { FFL("suppress-hierarchy"),no_argument,NULL,BG_ARG_SUPPRESS_HIERARCHY },
    { FFL("suppress-progress"),no_argument,NULL,BG_ARG_SUPPRESS_PROGRESS },
    // momentary //////////////////////////////////////////////////////////////
    { FFL("momentary-mean"),no_argument,NULL,
        FFL('i') },
    { FFL("momentary-maximum"),no_argument,NULL,
        FFL('m') },
    { FFL("momentary-range"),no_argument,NULL,
        BG_ARG_MOMENTARY_RANGE },
    { FFL("momentary-length"),required_argument,NULL,
        BG_ARG_MOMENTARY_LENGTH },
    { FFL("momentary-overlap"),required_argument,NULL,
        BG_ARG_MOMENTARY_OVERLAP },
    { FFL("momentary-mean-gate"),required_argument,NULL,
        BG_ARG_MOMENTARY_MEAN_GATE },
    { FFL("momentary-range-gate"),required_argument,NULL,
        BG_ARG_MOMENTARY_RANGE_GATE },
    { FFL("momentary-range-lower-bound"),required_argument,NULL,
        BG_ARG_MOMENTARY_RANGE_LOWER_BOUND },
    { FFL("momentary-range-upper-bound"),required_argument,NULL,
        BG_ARG_MOMENTARY_RANGE_UPPER_BOUND },
    // shortterm //////////////////////////////////////////////////////////////
    { FFL("shortterm-mean"),no_argument,NULL,
        BG_ARG_SHORTTERM_MEAN },
    { FFL("shortterm-maximum"),no_argument,NULL,
        FFL('s') },
    { FFL("shortterm-range"),no_argument,NULL,
        FFL('r') },
    { FFL("shortterm-length"),required_argument,NULL,
        BG_ARG_SHORTTERM_LENGTH },
    { FFL("shortterm-overlap"),required_argument,NULL,
        BG_ARG_SHORTTERM_OVERLAP },
    { FFL("shortterm-mean-gate"),required_argument,NULL,
        BG_ARG_SHORTTERM_MEAN_GATE },
    { FFL("shortterm-range-gate"),required_argument,NULL,
        BG_ARG_SHORTTERM_RANGE_GATE },
    { FFL("shortterm-range-lower-bound"),required_argument,NULL,
        BG_ARG_SHORTTERM_RANGE_LOWER_BOUND },
    { FFL("shortterm-range-upper-bound"),required_argument,NULL,
        BG_ARG_SHORTTERM_RANGE_UPPER_BOUND },
#if defined (BG_TRADITIONAL) // [
    { FFL("traditional"),no_argument,NULL,
        BG_ARG_TRADITIONAL },
#elif defined (BG_OPPOSITE) // [
    { FFL("opposite-sign"),no_argument,NULL,
        BG_ARG_OPPOSITE },
    { FFL("opposite"),no_argument,NULL,
        BG_ARG_OPPOSITE },
#endif // ]
#if defined (BG_LOUDNESS_ALL) // [
    { FFL("loudness-all"),no_argument,NULL,
        BG_ARG_LOUDNESS_ALL },
#endif // ]
    { 0 }
  };

  int code=1;
#if defined (_WIN32) // [
#if ! defined (BG_UNICODE) // [
  int utf16=0;
#endif // ]
  // if LANG is set to e.g. "en_US.UTF-8" we assume we're run from
  // e.g. MSYS2 shell undestanding UTF-8 otherwise from MS console using
  // codepage OEM.
  const char *lang=getenv("LANG");
#else // ] [
  const char *locale=NULL;
#endif // ]
  const ffchar_t *fpath=NULL;
  enum BGFlagHelpArg help_args=0;
#if defined (BG_PARAM_SCRIPT) // [
  ffchar_t *script=NULL;
#endif // ]
#if defined (HAVE_FF_DYNLOAD) && defined (__linux__) && defined (__GNUC__) // [
  const char *lhs;
  const char *rhs;
#endif // ]
  bg_param_t param;
  int c;
  double overlap;
  unsigned version;
#if (58<=LIBAVCODEC_VERSION_MAJOR||58<=LIBAVFORMAT_VERSION_MAJOR) // [
  void *opaque;
#endif // ]
  const AVCodec *codec;
  const AVOutputFormat *oformat;
#if defined (BG_CLOCK) // [
  double t1,t2,sec;
#endif // ]
  int verbose;

#if defined (_WIN32) // [
  /////////////////////////////////////////////////////////////////////////////
  SetConsoleOutputCP(CP_UTF8);
#endif // ]

  /////////////////////////////////////////////////////////////////////////////
  if (argc<2) {
    _DMESSAGE("missing command line arguments.");
    _FPRINTF(stderr,"\n");
    bg_usage(argv[0],stderr);
    goto e_arg;
  }

  /////////////////////////////////////////////////////////////////////////////
  if (bg_param_create(&param)<0) {
    _DMESSAGE("create parameters");
    goto e_arg;
  }

  /////////////////////////////////////////////////////////////////////////////
  while (0<=(c=FF_GETOPT_LONG(argc,argv,FLAGS,OPTS,NULL))) {
    switch (c) {
    ///////////////////////////////////////////////////////////////////////////
    case FFL('?'):
      _FPRINTF(stderr,"\n\n");
      bg_usage(argv[0],stderr);
      goto e_arg;
    ///////////////////////////////////////////////////////////////////////////
    case FFL('v'):
#if 0 // [
      bg_version(argv[0],stdout);
      code=0;
      goto e_arg;
#else // ] [
      help_args|=BG_FLAG_HELP_ARG_VERSION;
      break;
#endif // ]
    case FFL('h'):
      if (!optarg)
        help_args|=BG_FLAG_HELP_ARG_NULL;
      else if (!FFSTRCMP(FFL("codec"),optarg))
        help_args|=BG_FLAG_HELP_ARG_CODEC;
      else if (!FFSTRCMP(FFL("suffix"),optarg))
        help_args|=BG_FLAG_HELP_ARG_SUFFIX;
      else {
        _FPRINTFV(stderr,"Error: help topic \"%" PBU_PRIs "\" not"
            " recognized.\n\n",optarg);
        bg_usage(argv[0],stderr);
        goto e_arg;
      }

      break;
    case FFL('b'):
      param.interval.begin=bg_parse_time(optarg);
      break;
    case FFL('d'):
      param.interval.duration=bg_parse_time(optarg);
      break;
    case FFL('f'):
      fpath=optarg;
      break;
    case FFL('o'):
      if (!optarg) {
        _DMESSAGE("missing argument to option -o/--output");
        goto e_arg;
      }
      else if (param.overwrite) {
        _DMESSAGE("specify either option -o/--output or option --overwrite"
            " but not both");
        bg_usage(argv[0],stderr);
        goto e_arg;
      }

      param.output.dirname=bg_pathnorm(optarg);
      break;
    case FFL('u'):
      if (!FFSTRCASECMP(FFL("integrated"),optarg)
          ||!FFSTRCASECMP(FFL("momentary-mean"),optarg))
        param.flags.aggregate|=BG_FLAGS_AGG_MOMENTARY_MEAN;
      else if (!FFSTRCASECMP(FFL("momentary"),optarg)
          ||!FFSTRCASECMP(FFL("momentary-maximum"),optarg))
        param.flags.aggregate|=BG_FLAGS_AGG_MOMENTARY_MAXIMUM;
      else if (!FFSTRCASECMP(FFL("shortterm-mean"),optarg))
        param.flags.aggregate|=BG_FLAGS_AGG_SHORTTERM_MEAN;
      else if (!FFSTRCASECMP(FFL("shortterm"),optarg)
          ||!FFSTRCASECMP(FFL("shortterm-maximum"),optarg))
        param.flags.aggregate|=BG_FLAGS_AGG_SHORTTERM_MAXIMUM;
      else {
        _DMESSAGEV("method \"%" PBU_PRIs "\" not recognized",optarg);
        bg_usage(argv[0],stderr);
        goto e_arg;
      }

      break;
    /// without argument ////////////////////////////////////////////////////
    case FFL('a'):
      param.flags.mode|=BG_FLAGS_MODE_APPLY;

      if (optarg) {
        param.weight.enabled=1;
        param.weight.value=FFATOF(optarg);

        if (param.weight.value<0.0||1.0<param.weight.value) {
          _DMESSAGE("weight out of range");
          bg_usage(argv[0],stderr);
          goto e_arg;
        }
      }

      break;
    case FFL('i'):
      param.flags.aggregate|=BG_FLAGS_AGG_MOMENTARY_MEAN;
      break;
    case FFL('s'):
      param.flags.aggregate|=BG_FLAGS_AGG_SHORTTERM_MAXIMUM;
      break;
    case FFL('m'):
      param.flags.aggregate|=BG_FLAGS_AGG_MOMENTARY_MAXIMUM;
      break;
    case FFL('r'):
      param.flags.aggregate|=BG_FLAGS_AGG_SHORTTERM_RANGE;
      break;
    case FFL('p'):
      param.flags.aggregate|=BG_FLAGS_AGG_SAMPLEPEAK;
      break;
    case FFL('t'):
      param.flags.aggregate|=BG_FLAGS_AGG_TRUEPEAK;

#if defined (BG_SAMPLES_COUNT) && ! defined (UPSAMPLER_THRESHOULD) // [
      if (optarg)
        param.upsampler.threshould=FFATOI(optarg);
#endif // ]

      break;
#if defined (BG_PARAM_QUIET) // [
    case BG_ARG_QUIET:
      param.quiet=1;
      break;
#endif // ]
#if defined (BG_PARAM_THREADS) // [
    case BG_ARG_NTHREADS:
      param.nthreads=FFATOI(optarg);
      break;
#endif // ]
#if defined (BG_PARAM_SLEEP) // [
    case BG_ARG_SLEEP:
      param.sleep=FFATOI(optarg);
      break;
#endif // ]
    case FFL('x'):
    case BG_ARG_EXTENSION:
      if (!optarg) {
        param.flags.aggregate|=BG_FLAGS_AGG_TRUEPEAK;
        param.flags.extension=BG_FLAGS_EXT_ALL;
#if defined (BG_PARAM_STEREO) // [
        param.stereo=1;
#else // ] [
        param.decode.request.channel_layout=AV_CH_LAYOUT_STEREO;
#endif // ]
        param.decode.drc.enabled=1;
#if 0 // [
        param.decode.drc.scale=0.0;
#endif // ]
      }
      else if (0==FFSTRCASECMP(FFL("2"),optarg)) {
        param.flags.aggregate|=BG_FLAGS_AGG_TRUEPEAK;
        param.flags.extension=BG_FLAGS_EXT_ALL&~BG_FLAGS_EXT_RENAME;
#if defined (BG_PARAM_STEREO) // [
        param.stereo=1;
#else // ] [
        param.decode.request.channel_layout=AV_CH_LAYOUT_STEREO;
#endif // ]
        param.decode.drc.enabled=1;
#if 0 // [
        param.decode.drc.scale=0.0;
#endif // ]
      }
      else if (0==FFSTRCASECMP(FFL("rename"),optarg))
        param.flags.extension|=BG_FLAGS_EXT_RENAME;
      else if (0==FFSTRCASECMP(FFL("csv"),optarg))
        param.flags.extension|=BG_FLAGS_EXT_CSV;
      else if (0==FFSTRCASECMP(FFL("copy"),optarg))
        param.flags.extension|=BG_FLAGS_EXT_COPY;
      else if (0==FFSTRCASECMP(FFL("tags"),optarg))
        param.flags.extension|=BG_FLAGS_EXT_TAGS;
      else if (0==FFSTRCASECMP(FFL("all"),optarg))
        param.flags.extension|=BG_FLAGS_EXT_ALL;
      else {
        _DMESSAGEV("extension \"%" PBU_PRIs "\" not recognized",optarg);
        bg_usage(argv[0],stderr);
        goto e_arg;
      }

      break;
    /// without flag ////////////////////////////////////////////////////////
    case BG_ARG_AUDIO:
      param.ai=FFATOI(optarg);
      break;
#if defined (FF_SERGEY_INDEX_BUGFIX) // [
    case BG_ARG_VIDEO:
      param.vi=FFATOI(optarg);
      break;
#endif // ]
    case BG_ARG_UNIT:
      if (!FFSTRCMP(optarg,FFL("ebu")))
        bg_param_set_unit_ebu(&param);
      else if (!FFSTRCMP(optarg,FFL("db")))
        bg_param_set_unit_db(&param);
      else {
        _DMESSAGEV("argument to option --unit not recognized:"
            " \"%" PBU_PRIs "\"",optarg);
        bg_usage(argv[0],stderr);
        goto e_arg;
      }

      break;
#if defined (_WIN32) && ! defined (BG_UNICODE) // [
    case BG_ARG_UTF_16:
      utf16=1;

      if (optarg)
        fpath=optarg;

      break;
#endif // ]
#if defined (BG_PARAM_REFERENCE) // [
    case BG_REFERENCE:
      param.reference=optarg;
      break;
#endif // ]
#if defined (BG_PARAM_SKIP_SCAN) // [
    case BG_SKIP_SCAN:
      param.skip_scan=1;
      break;
#endif // ]
#if defined (BG_PARAM_SCRIPT) // [
    case BG_SCRIPT:
      script=optarg;
      break;
#endif // ]
#if defined (BG_SAMPLES_COUNT) && defined (UPSAMPLER_THRESHOULD) // [
    case BG_UPSAMPLER_THRESHOULD:
#if defined (_WIN32) // [
      param.upsampler.threshould=_wtoi(optarg);
#else // ] [
      param.upsampler.threshould=atoi(optarg);
#endif // ]
      break;
#endif // ]
#if defined (BG_PARAM_SHELL) // [
    case BG_SHELL:
      param.shell.interpreter=optarg;
      break;
#endif // ]
    case BG_ARG_DRC:
      param.decode.drc.enabled=1;
      param.decode.drc.scale=FFATOF(optarg);
      break;
    case BG_ARG_SUFFIX:
      param.out.sfx=optarg;
      break;
    case BG_ARG_TAG_PREFIX:
#if defined (_WIN32) // [
      ff_wcs2str(optarg,param.tag.pfx,CP_UTF8,(sizeof param.tag.pfx)-2);
#else // ] [
      strncpy(param.tag.pfx,optarg,(sizeof param.tag.pfx)-2);
#endif // ]
      param.tag.pfx[strlen(param.tag.pfx)+1]='\0';
      param.tag.pfx[strlen(param.tag.pfx)]='_';
      break;
    case BG_ARG_OVERWRITE:
      if (param.output.dirname) {
        _DMESSAGE("specify either option -o/--output or option --overwrite"
            " but not both");
        bg_usage(argv[0],stderr);
        goto e_arg;
      }

      param.overwrite=1;
      break;
#if defined (BG_PARAM_LFE) // [
    case BG_ARG_CH_LFE:
      param.lfe=FFATOI(optarg);
      break;
#endif // ]
    case BG_ARG_OUTPUT_CODEC:
#if defined (_WIN32) // [
      ff_wcs2str(optarg,param.codec.name,CP_UTF8,(sizeof param.codec.name)-1);
#else // ] [
      param.codec.name=optarg;
#endif // ]
      break;
    case BG_ARG_TEMP_PREFIX:
      if (!*optarg) {
        _DMESSAGE("missing argument to --temp-prefix");
        bg_usage(argv[0],stderr);
        goto e_arg;
      }

      param.temp_prefix=optarg;
      break;
    case BG_ARG_SUPPRESS_HIERARCHY:
      param.suppress.hierarchy=1;
      break;
    case BG_ARG_SUPPRESS_PROGRESS:
      param.suppress.progress=1;
      break;
    case BG_ARG_LOGLEVEL:
      if (0==FFSTRCASECMP(FFL("quiet"),optarg))
        param.loglevel=AV_LOG_QUIET;
      else if (0==FFSTRCASECMP(FFL("panic"),optarg))
        param.loglevel=AV_LOG_PANIC;
      else if (0==FFSTRCASECMP(FFL("fatal"),optarg))
        param.loglevel=AV_LOG_FATAL;
      else if (0==FFSTRCASECMP(FFL("error"),optarg))
        param.loglevel=AV_LOG_ERROR;
      else if (0==FFSTRCASECMP(FFL("warning"),optarg))
        param.loglevel=AV_LOG_WARNING;
      else if (0==FFSTRCASECMP(FFL("info"),optarg))
        param.loglevel=AV_LOG_INFO;
      else if (0==FFSTRCASECMP(FFL("verbose"),optarg))
        param.loglevel=AV_LOG_VERBOSE;
      else if (0==FFSTRCASECMP(FFL("debug"),optarg))
        param.loglevel=AV_LOG_DEBUG;
      else if (0==FFSTRCASECMP(FFL("trace"),optarg))
        param.loglevel=AV_LOG_TRACE;
      else {
        _FPRINTFV(stderr,"Error: loglevel \"%" PBU_PRIs "\""
            " not recognized.\n\n",optarg);
        bg_usage(argv[0],stderr);
        goto e_arg;
      }

  		//av_log_set_level(param.loglevel);
      break;
    case BG_ARG_NORM:
      param.flags.norm=BG_FLAGS_NORM_NULL;
      param.norm=FFATOF(optarg);
      break;
#if defined (BG_CLOCK) // [
    case BG_ARG_TIME:
      param.time=1;
      break;
#endif // ]
    case BG_ARG_PREAMP:
      param.preamp=FFATOF(optarg);
      break;
    /// without flag ////////////////////////////////////////////////////////
    case BG_ARG_ATSC:
      param.flags.norm=BG_FLAGS_NORM_ATSC;
      param.norm=-24.0;
      bg_param_set_unit_db(&param);
      break;
    case BG_ARG_EBU:
      param.flags.norm=BG_FLAGS_NORM_EBU;
      param.norm=-23.0;
      bg_param_set_unit_ebu(&param);
      break;
    case BG_ARG_REPLAYGAIN:
      param.flags.norm=BG_FLAGS_NORM_REPLAYGAIN;
      param.norm=-18.0;
      bg_param_set_unit_db(&param);
      break;
#if defined (BG_BWF_TAGS) // [
    case BG_ARG_TAGS_RG:
      param.flags.mode|=BG_FLAGS_MODE_TAGS_RG;
      break;
    case BG_ARG_TAGS_BWF:
      param.flags.mode|=BG_FLAGS_MODE_TAGS_BWF;
      break;
#endif // ]
    case BG_ARG_TAGS_TRACK:
      param.flags.mode|=BG_FLAGS_MODE_TAGS_TRACK;
      break;
    case BG_ARG_TAGS_ALBUM:
      param.flags.mode|=BG_FLAGS_MODE_TAGS_ALBUM;
      break;
#if defined (HAVE_BG_XML) // [
    case BG_ARG_XML:
			bg_set_output_xml(&param);

#if defined (BG_PARAM_XML_CDATA) // [
      if (optarg) {
        if (FFSTRCASECMP(FFL("cdata"),optarg)) {
          _DMESSAGEV("argument  \"%" PBU_PRIs "\" to option \"--xml\" not"
              " supported",optarg);
          bg_usage(argv[0],stderr);
          goto e_arg;
        }
        
        param.xml.cdata=1;
      }
      else
        param.xml.cdata=0;
#endif // ]
      break;
#endif // ]
    case BG_ARG_CSV:
      if (optarg) {
        if (1!=FFSTRLEN(optarg)) {
          _DMESSAGEV("argument  \"%" PBU_PRIs "\" to option \"--csv\" not"
              " supported",optarg);
          bg_usage(argv[0],stderr);
          goto e_arg;
        }

        param.csv.separator=optarg[0];
      }

      param.print.vmt=&bg_print_csv_vmt;
      break;
#if defined (FF_INPUT_LIST) // [
    case FFL('l'):
      param.list.in=1;

      if (optarg) {
        if (!optarg||!FFSTRCASECMP(FFL("out"),optarg))
          param.list.out=1;
        else {
          _DMESSAGEV("argument  \"%" PBU_PRIs "\" to option \"--list\" not"
              " supported",optarg);
          bg_usage(argv[0],stderr);
          goto e_arg;
        }
      }

      break;
#endif // ]
    case BG_ARG_MATRIX:
      if (!FFSTRCASECMP(FFL("front-left"),optarg))
        param.decode.request.channel_layout=AV_CH_FRONT_LEFT;
      else if (!FFSTRCASECMP(FFL("front-right"),optarg))
        param.decode.request.channel_layout=AV_CH_FRONT_RIGHT;
      else if (!FFSTRCASECMP(FFL("front-center"),optarg))
        param.decode.request.channel_layout=AV_CH_FRONT_CENTER;
      else if (!FFSTRCASECMP(FFL("low-frequency"),optarg))
        param.decode.request.channel_layout=AV_CH_LOW_FREQUENCY;
      else if (!FFSTRCASECMP(FFL("back-left"),optarg))
        param.decode.request.channel_layout=AV_CH_BACK_LEFT;
      else if (!FFSTRCASECMP(FFL("back-right"),optarg))
        param.decode.request.channel_layout=AV_CH_BACK_RIGHT;
      else if (!FFSTRCASECMP(FFL("front-left-of-center"),optarg))
        param.decode.request.channel_layout=AV_CH_FRONT_LEFT_OF_CENTER;
      else if (!FFSTRCASECMP(FFL("front-right-of-center"),optarg))
        param.decode.request.channel_layout=AV_CH_FRONT_RIGHT_OF_CENTER;
      else if (!FFSTRCASECMP(FFL("back-center"),optarg))
        param.decode.request.channel_layout=AV_CH_BACK_CENTER;
      else if (!FFSTRCASECMP(FFL("side-left"),optarg))
        param.decode.request.channel_layout=AV_CH_SIDE_LEFT;
      else if (!FFSTRCASECMP(FFL("side-right"),optarg))
        param.decode.request.channel_layout=AV_CH_SIDE_RIGHT;
      else if (!FFSTRCASECMP(FFL("top-center"),optarg))
        param.decode.request.channel_layout=AV_CH_TOP_CENTER;
      else if (!FFSTRCASECMP(FFL("top-front-left"),optarg))
        param.decode.request.channel_layout=AV_CH_TOP_FRONT_LEFT;
      else if (!FFSTRCASECMP(FFL("top-front-center"),optarg))
        param.decode.request.channel_layout=AV_CH_TOP_FRONT_CENTER;
      else if (!FFSTRCASECMP(FFL("top-front-right"),optarg))
        param.decode.request.channel_layout=AV_CH_TOP_FRONT_RIGHT;
      else if (!FFSTRCASECMP(FFL("top-back-left"),optarg))
        param.decode.request.channel_layout=AV_CH_TOP_BACK_LEFT;
      else if (!FFSTRCASECMP(FFL("top-back-center"),optarg))
        param.decode.request.channel_layout=AV_CH_TOP_BACK_CENTER;
      else if (!FFSTRCASECMP(FFL("top-back-right"),optarg))
        param.decode.request.channel_layout=AV_CH_TOP_BACK_RIGHT;
      else if (!FFSTRCASECMP(FFL("stereo-left"),optarg))
        param.decode.request.channel_layout=AV_CH_STEREO_LEFT;
      else if (!FFSTRCASECMP(FFL("stereo-right"),optarg))
        param.decode.request.channel_layout=AV_CH_STEREO_RIGHT;
      else if (!FFSTRCASECMP(FFL("wide-left"),optarg))
        param.decode.request.channel_layout=AV_CH_WIDE_LEFT;
      else if (!FFSTRCASECMP(FFL("wide-right"),optarg))
        param.decode.request.channel_layout=AV_CH_WIDE_RIGHT;
      else if (!FFSTRCASECMP(FFL("surround-direct-left"),optarg))
        param.decode.request.channel_layout=AV_CH_SURROUND_DIRECT_LEFT;
      else if (!FFSTRCASECMP(FFL("surround-direct-right"),optarg))
        param.decode.request.channel_layout=AV_CH_SURROUND_DIRECT_RIGHT;
      else if (!FFSTRCASECMP(FFL("low-frequency-2"),optarg))
        param.decode.request.channel_layout=AV_CH_LOW_FREQUENCY_2;
      else if (!FFSTRCASECMP(FFL("layout-mono"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_MONO;
      else if (!FFSTRCASECMP(FFL("layout-stereo"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_STEREO;
      else if (!FFSTRCASECMP(FFL("layout-2point1"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_2POINT1;
      else if (!FFSTRCASECMP(FFL("layout-2-1"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_2_1;
      else if (!FFSTRCASECMP(FFL("layout-surround"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_SURROUND;
      else if (!FFSTRCASECMP(FFL("layout-3point1"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_3POINT1;
      else if (!FFSTRCASECMP(FFL("layout-4point0"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_4POINT0;
      else if (!FFSTRCASECMP(FFL("layout-4point1"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_4POINT1;
      else if (!FFSTRCASECMP(FFL("layout-2-2"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_2_2;
      else if (!FFSTRCASECMP(FFL("layout-quad"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_QUAD;
      else if (!FFSTRCASECMP(FFL("layout-5point0"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_5POINT0;
      else if (!FFSTRCASECMP(FFL("layout-5point1"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_5POINT1;
      else if (!FFSTRCASECMP(FFL("layout-5point0-back"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_5POINT0_BACK;
      else if (!FFSTRCASECMP(FFL("layout-5point1-back"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_5POINT1_BACK;
      else if (!FFSTRCASECMP(FFL("layout-6point0"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_6POINT0;
      else if (!FFSTRCASECMP(FFL("layout-6point0-front"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_6POINT0_FRONT;
      else if (!FFSTRCASECMP(FFL("layout-hexagonal"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_HEXAGONAL;
      else if (!FFSTRCASECMP(FFL("layout-6point1"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_6POINT1;
      else if (!FFSTRCASECMP(FFL("layout-6point1-back"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_6POINT1_BACK;
      else if (!FFSTRCASECMP(FFL("layout_6point1_front"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_6POINT1_FRONT;
      else if (!FFSTRCASECMP(FFL("layout-7point0"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_7POINT0;
      else if (!FFSTRCASECMP(FFL("layout-7point0-front"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_7POINT0_FRONT;
      else if (!FFSTRCASECMP(FFL("layout-7point1"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_7POINT1;
      else if (!FFSTRCASECMP(FFL("layout-7point1-wide"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_7POINT1_WIDE;
      else if (!FFSTRCASECMP(FFL("layout-7point1-wide-back"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_7POINT1_WIDE_BACK;
      else if (!FFSTRCASECMP(FFL("layout-octagonal"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_OCTAGONAL;
      else if (!FFSTRCASECMP(FFL("layout-hexadecagonal"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_HEXADECAGONAL;
      else if (!FFSTRCASECMP(FFL("layout-stereo-downmix"),optarg))
        param.decode.request.channel_layout=AV_CH_LAYOUT_STEREO_DOWNMIX;
      else {
        _DMESSAGEV("argument to option --matrix not recognized:"
            " \"%" PBU_PRIs "\"",optarg);
        bg_usage(argv[0],stderr);
        goto e_arg;
      }

      break;
    case BG_ARG_STEREO:
#if defined (BG_PARAM_STEREO) // [
      param.stereo=1;
#else // ] [
      param.decode.request.channel_layout=AV_CH_LAYOUT_STEREO;
#endif // ]
      break;
    /////////////////////////////////////////////////////////////////////////
    case BG_ARG_MOMENTARY_RANGE:
      param.flags.aggregate|=BG_FLAGS_AGG_MOMENTARY_RANGE;
      break;
    ////////
    case BG_ARG_MOMENTARY_LENGTH:
      param.momentary.ms=FFATOF(optarg);
      break;
    case BG_ARG_MOMENTARY_OVERLAP:
      overlap=FFATOF(optarg);

      if (0.0<=overlap&&overlap<100.0)
        param.momentary.partition=floor(100.0/(100.0-overlap)+0.5);
      else {
        _FPRINTF(stderr,"Error: overlap out of range [0..100).\n\n");
        bg_usage(argv[0],stderr);
        goto e_arg;
      }

      break;
    case BG_ARG_MOMENTARY_MEAN_GATE:
      param.momentary.mean.gate=FFATOF(optarg);
      break;
    case BG_ARG_MOMENTARY_RANGE_GATE:
      param.momentary.range.gate=FFATOF(optarg);
      break;
    case BG_ARG_MOMENTARY_RANGE_LOWER_BOUND:
      param.momentary.range.lower_bound=FFATOF(optarg);
      break;
    case BG_ARG_MOMENTARY_RANGE_UPPER_BOUND:
      param.momentary.range.upper_bound=FFATOF(optarg);
      break;
    /////////////////////////////////////////////////////////////////////////
    case BG_ARG_SHORTTERM_MEAN:
      param.flags.aggregate|=BG_FLAGS_AGG_SHORTTERM_MEAN;
      break;
    ////////
    case BG_ARG_SHORTTERM_LENGTH:
      param.shortterm.ms=FFATOF(optarg);
      break;
    case BG_ARG_SHORTTERM_OVERLAP:
      overlap=FFATOF(optarg);

      if (0.0<=overlap&&overlap<100.0)
        param.shortterm.partition=floor(100.0/(100.0-overlap)+0.5);
      else {
        _FPRINTF(stderr,"Error: overlap out of range [0..100).\n\n");
        bg_usage(argv[0],stderr);
        goto e_arg;
      }

      break;
    case BG_ARG_SHORTTERM_MEAN_GATE:
      param.shortterm.mean.gate=FFATOF(optarg);
      break;
    case BG_ARG_SHORTTERM_RANGE_GATE:
      param.shortterm.range.gate=FFATOF(optarg);
      break;
    case BG_ARG_SHORTTERM_RANGE_LOWER_BOUND:
      param.shortterm.range.lower_bound=FFATOF(optarg);
      break;
    case BG_ARG_SHORTTERM_RANGE_UPPER_BOUND:
      param.shortterm.range.upper_bound=FFATOF(optarg);
      break;
#if defined (BG_TRADITIONAL) // [
    case BG_ARG_TRADITIONAL:
      param.traditional=1;
      break;
#elif defined (BG_OPPOSITE) // [
    case BG_ARG_OPPOSITE:
      param.opposite=1;
      break;
#endif // ]
#if defined (BG_LOUDNESS_ALL) // [
    case BG_ARG_LOUDNESS_ALL:
      param.flags.aggregate|=BG_FLAGS_AGG_MOMENTARY_MEAN;
      param.flags.aggregate|=BG_FLAGS_AGG_SHORTTERM_MAXIMUM;
      param.flags.aggregate|=BG_FLAGS_AGG_SHORTTERM_MEAN;
      param.flags.aggregate|=BG_FLAGS_AGG_MOMENTARY_MAXIMUM;
      break;
#endif // ]
    default:
#if defined (_WIN32) // [
      if ('c'==optopt)
        _DMESSAGEV("option -%C requires an argument",optopt);
      else if (iswprint(optopt))
        _DMESSAGEV("unknown option -%C",optopt);
      else
        _DMESSAGEV("unknown option character \\x%X",optopt);
#else // ] [
      if ('c'==optopt)
        _DMESSAGEV("option -%c requires an argument",optopt);
      else if (isprint(optopt))
        _DMESSAGEV("unknown option -%c",optopt);
      else
        _DMESSAGEV("unknown option character \\x%x",optopt);
#endif // ]

      bg_usage(argv[0],stderr);
      goto e_arg;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
#if 0 // [
  if (BG_FLAG_HELP_ARG_NULL&help_args) {
    bg_usage(argv[0],stdout);

    if (BG_FLAG_HELP_ARG_NULL==help_args) {
      code=0;
      goto e_arg;
    }
  }
#else // ] [
  if (BG_FLAG_HELP_ARG_VERSION&help_args) {
    bg_version(argv[0],stdout);
    fflush(stdout);

    if (BG_FLAG_HELP_ARG_VERSION==help_args) {
      code=0;
      goto e_arg;
    }
  }
  else if (BG_FLAG_HELP_ARG_NULL&help_args) {
    bg_usage(argv[0],stdout);
    fflush(stdout);

    if (BG_FLAG_HELP_ARG_NULL==help_args) {
      code=0;
      goto e_arg;
    }
  }
#endif // ]

#if defined (BG_PARAM_SCRIPT) // [
  if (script) {
    if (!param.output.dirname&&!param.overwrite) {
      _FPRINTF(stderr,"Error: option --script requires option -o/--output"
          " or option --overwrite.\n\n");
      bg_usage(argv[0],stderr);
      goto e_arg;
    }

    param.script=script;
    param.flags.mode|=BG_FLAGS_MODE_APPLY;
  }
#endif // ]

  // in case no class of tags is given we provide both, i.e. album and
  // track tags.
  if (!(param.flags.mode&BG_FLAGS_MODE_TAGS_ALL))
    param.flags.mode|=BG_FLAGS_MODE_TAGS_ALL;

#if defined (HAVE_FF_DYNLOAD) // [
#if defined (__linux__) && defined (__GNUC__) // [
  /////////////////////////////////////////////////////////////////////////////
  lhs=gnu_get_libc_version();
  rhs=BG_GNU_LIBC_VERSION;

  if (strcmpex(&lhs,&rhs,'.')<0||strcmpex(&lhs,&rhs,0)<0) {
#if 0 // [
    _DWARNINGV("libc might be out-dated: expecting %s, found %s",
        BG_GNU_LIBC_VERSION,gnu_get_libc_version());
    _FPRINTF(stderr,"  Might prevent loading FFmpeg shared objects."
        " Good luck!\n");
#else // ] [
    _DMESSAGEV("Attmpting to dynamically load FFmpeg shared objects by means"
        " of an out-dated libc: expecting %s, found %s",BG_GNU_LIBC_VERSION,
        gnu_get_libc_version());
    goto e_libc;
#endif // ]
  }
#endif // ]

  /////////////////////////////////////////////////////////////////////////////
  // load the FFmpeg libraries from "bs1770gain-tools".
  if (ff_dynload(TOOLS)<0) {
    _DMESSAGE("loading shared libraries");
    goto e_dynload;
  }
#endif // ]

  /////////////////////////////////////////////////////////////////////////////
  version=avutil_version();

  if (LIBAVUTIL_VERSION_MAJOR!=AV_VERSION_MAJOR(version)) {
#if defined (HAVE_FF_DYNLOAD) // [
    _DMESSAGEV("wrong version of avutil: expecting %d, found %d at \"%s\"",
        LIBAVUTIL_VERSION_MAJOR,AV_VERSION_MAJOR(version),ff_dynload_path());
#else // ] [
    _DMESSAGEV("wrong version of avutil: expecting %d, linked %d",
        LIBAVUTIL_VERSION_MAJOR,AV_VERSION_MAJOR(version));
#endif // ]
    goto e_versions;
  }

  ////////
  version=swresample_version();

  if (LIBSWRESAMPLE_VERSION_MAJOR!=AV_VERSION_MAJOR(version)) {
#if defined (HAVE_FF_DYNLOAD) // [
    _DMESSAGEV("wrong version of swresample: expecting %d, found %d at \"%s\"",
        LIBSWRESAMPLE_VERSION_MAJOR,AV_VERSION_MAJOR(version),
        ff_dynload_path());
#else // ] [
    _DMESSAGEV("wrong version of swresample: expecting %d, linked %d",
        LIBSWRESAMPLE_VERSION_MAJOR,AV_VERSION_MAJOR(version));
#endif // ]
    goto e_versions;
  }

  ////////
  version=avcodec_version();

  if (LIBAVCODEC_VERSION_MAJOR!=AV_VERSION_MAJOR(version)) {
#if defined (HAVE_FF_DYNLOAD) // [
    _DMESSAGEV("wrong version of avcodec: expecting %d, found %d at \"%s\"",
        LIBAVCODEC_VERSION_MAJOR,AV_VERSION_MAJOR(version),ff_dynload_path());
#else // ] [
    _DMESSAGEV("wrong version of avcodec: expecting %d, linked %d",
        LIBAVCODEC_VERSION_MAJOR,version);
#endif // ]
    goto e_versions;
  }

  ////////
  version=avformat_version();

  if (LIBAVFORMAT_VERSION_MAJOR!=AV_VERSION_MAJOR(version)) {
#if defined (HAVE_FF_DYNLOAD) // [
    _DMESSAGEV("wrong version of avformat: expecting %d, found %d at \"%s\"",
        LIBAVFORMAT_VERSION_MAJOR,AV_VERSION_MAJOR(version),ff_dynload_path());
#else // ] [
    _DMESSAGEV("wrong version of avformat: expecting %d, linked %d",
        LIBAVFORMAT_VERSION_MAJOR,AV_VERSION_MAJOR(version));
#endif // ]
    goto e_versions;
  }

  ////////
  version=swscale_version();

  if (LIBSWSCALE_VERSION_MAJOR!=AV_VERSION_MAJOR(version)) {
#if defined (HAVE_FF_DYNLOAD) // [
    _DMESSAGEV("wrong version of swscale: expecting %d, found %d at \"%s\"",
        LIBSWSCALE_VERSION_MAJOR,AV_VERSION_MAJOR(version),ff_dynload_path());
#else // ] [
    _DMESSAGEV("wrong version of swscale: expecting %d, linked %d",
        LIBSWSCALE_VERSION_MAJOR,AV_VERSION_MAJOR(version));
#endif // ]
    goto e_versions;
  }

  ////////
  version=postproc_version();

  if (LIBPOSTPROC_VERSION_MAJOR!=AV_VERSION_MAJOR(version)) {
#if defined (HAVE_FF_DYNLOAD) // [
    _DMESSAGEV("wrong version of postproc: expecting %d, found %d at \"%s\"",
        LIBPOSTPROC_VERSION_MAJOR,AV_VERSION_MAJOR(version),ff_dynload_path());
#else // ] [
    _DMESSAGEV("wrong version of postproc: expecting %d, linked %d",
        LIBPOSTPROC_VERSION_MAJOR,AV_VERSION_MAJOR(version));
#endif // ]
    goto e_versions;
  }

  ////////
  version=avfilter_version();

  if (LIBAVFILTER_VERSION_MAJOR!=AV_VERSION_MAJOR(version)) {
#if defined (HAVE_FF_DYNLOAD) // [
    _DMESSAGEV("wrong version of avfilter: %d needed, %d loaded from \"%s\"",
        LIBAVFILTER_VERSION_MAJOR,AV_VERSION_MAJOR(version),ff_dynload_path());
#else // ] [
    _DMESSAGEV("wrong version of avfilter: expecting %d, linked %d",
        LIBAVFILTER_VERSION_MAJOR,AV_VERSION_MAJOR(version));
#endif // ]
    goto e_versions;
  }

  /////////////////////////////////////////////////////////////////////////////
#if (LIBAVFORMAT_VERSION_MAJOR<58) // [
  av_register_all();
#endif // ]
#if (LIBAVFILTER_VERSION_MAJOR<7) // [
  avfilter_register_all();
#endif // ]

  if (help_args) {
    ///////////////////////////////////////////////////////////////////////////
    if (BG_FLAG_HELP_ARG_SUFFIX&help_args) {
      /////////////////////////////////////////////////////////////////////////
      _FPRINTF(stdout,"available arguments for option --suffix:\n");
#if (LIBAVFORMAT_VERSION_MAJOR<58) // [
      // https://stackoverflow.com/questions/2940521/where-to-get-full-list-of-libav-formats
      oformat=av_oformat_next(NULL);

      while (oformat) {
        if (oformat->audio_codec&&oformat->extensions)
          _FPRINTFV(stdout,"  %s\n",oformat->extensions);

        oformat=av_oformat_next(oformat);
      }
#else // ] [
      opaque=NULL;

      for (;;) {
        oformat=av_muxer_iterate(&opaque);

        if (!oformat)
          break;

        if (oformat->audio_codec&&oformat->extensions)
          _FPRINTFV(stdout,"  %s\n",oformat->extensions);
      }
#endif // ]

      fflush(stdout);
    }

    if (BG_FLAG_HELP_ARG_CODEC&help_args) {
      /////////////////////////////////////////////////////////////////////////
      _FPRINTF(stdout,"available arguments for option --codec:\n");
#if (LIBAVCODEC_VERSION_MAJOR<58) // [
      // https://stackoverflow.com/questions/2940521/where-to-get-full-list-of-libav-formats
      codec=av_codec_next(NULL);

      while (codec) {
        if (AVMEDIA_TYPE_AUDIO==codec->type)
          _FPRINTFV(stdout,"  %s\n",codec->name);

        codec=av_codec_next(codec);
      }
#else // ] [
      opaque=NULL;

      for (;;) {
        codec=av_codec_iterate(&opaque);

        if (!codec)
          break;

        if (AVMEDIA_TYPE_AUDIO==codec->type)
          _FPRINTFV(stdout,"  %s\n",codec->name);
      }
#endif // ]

      fflush(stdout);
    }

    ///////////////////////////////////////////////////////////////////////////
    code=0;
    goto e_help;
  }

#if defined (BG_PARAM_THREADS) // [
  /////////////////////////////////////////////////////////////////////////////
  if (param.nthreads<0)
    param.nthreads=0;
  else if (param.nthreads) {
    if (bg_param_threads_create(&param.threads,param.nthreads)<0) {
      _DMESSAGE("creating threads");
      goto e_threads;
    }
  }
#endif // ]

  /////////////////////////////////////////////////////////////////////////////
#if defined (_WIN32) // [
#if defined (BG_WIN32_CREATE_LOCALE) // [
  param.locale=_create_locale(LC_ALL,"");
#endif // ]
  // needed in order for _wcslwr() working as expected.
#if 1 // [
  _wsetlocale(LC_ALL,L"English_US");
#else // ] [
  _wsetlocale(LC_ALL,L"");
#endif // ]
  // if LANG is set to e.g. "en_US.UTF-8" we assume we're run from
  // e.g. MSYS2 shell undestanding UTF-8 otherwise from MS console using
  // codepage OEM. In the latter case we need an OEM representation of
  // e.g. basename.
  param.oem=!lang||!strstr(lang,"UTF-8");
#else // ] [
  // If not already active we set locale to an arbitrary UTF-8.
  locale=setlocale(LC_ALL,NULL);

  if (strstr(locale,"utf")||strstr(locale,"UTF"))
    locale=NULL;
  else
    setlocale(LC_ALL,"en_US.UTF-8");
#endif // ]


  /////////////////////////////////////////////////////////////////////////////
  if (fpath) {
#if defined (_WIN32) // [
    _wremove(fpath);

#if defined (BG_UNICODE) // [
    // UTF-16!
    param.result.f=_wfopen(fpath,L"wt,ccs=UNICODE");
    param.print.vmt->encoding(&param,16);
#else // ] [
    if (utf16) {
      //param.result.f=_wfopen(fpath,L"wt,ccs=UTF-16LE");
      param.result.f=_wfopen(fpath,L"wt,ccs=UNICODE");
      param.print.vmt->encoding(&param,16);
    }
    else {
      // If the file is encoded as UTF-8, then UTF-16 data is translated
      // into UTF-8 when it is written
      param.result.f=_wfopen(fpath,L"wt,ccs=UTF-8");
      param.print.vmt->encoding(&param,8);
    }
#endif // ]
#else // ] [
    remove(fpath);
    param.result.f=fopen(fpath,"w");
    param.print.vmt->encoding(&param,8);
#endif // ]

    if (!param.result.f) {
#if defined (_WIN32) // [
      _DMESSAGEV("opening file \"%S\"",fpath);
#else // ] [
      _DMESSAGEV("opening file \"%s\"",fpath);
#endif // ]
      goto e_file;
    }
  }
#if defined (BG_PARAM_DUMP) // [
  else if (param.dump)
    param.result.f=stderr;
#endif // ]

  /////////////////////////////////////////////////////////////////////////////
  av_log_set_level(param.loglevel);

  /////////////////////////////////////////////////////////////////////////////
  if (bg_param_alloc_arguments(&param,argc-optind)<0) {
    _FPRINTFV(stderr,"Error: setting arguments. (%s,%d,%s)\n\n",
				pbu_basename(__FILE__),__LINE__,__func__);
    bg_usage(argv[0],stderr);
    goto e_arguments;
  }

  /////////////////////////////////////////////////////////////////////////////
#if defined (BG_PARAM_QUIET) // [
  verbose=!param.quiet&&!param.suppress.progress;
#else // ] [
  verbose=!param.suppress.progress;
#endif // ]

#if defined (BG_PARAM_SKIP_SCAN) // [
  if (!param.skip_scan&&verbose) {
#else // ] [
  if (verbose) {
#endif // ]
		if (bg_is_output_xml(&param))
      _FPRINTF(stdout,"<!-- ");

    _FPRINTF(stdout,"scanning ");
    fflush(stdout);
#if ! defined (BG_PARAM_SKIP_SCAN) // [
  }
#else // ] [
  }
#endif // ]

#if defined (BG_CLOCK) // [
  /////////////////////////////////////////////////////////////////////////////
  t1=clock();
#endif // ]

  /////////////////////////////////////////////////////////////////////////////
#if defined (BG_PARAM_SKIP_SCAN) // [
  if (!param.skip_scan&&bg_param_loop(&param,argv+optind)<0) {
#else // ] [
  if (bg_param_loop(&param,argv+optind)<0) {
#endif // ]
    _DMESSAGE("counting");
    goto e_count;
#if ! defined (BG_PARAM_SKIP_SCAN) // [
  }
#else // ] [
  }
#endif // ]

#if defined (BG_PARAM_THREADS) // [
  if (0<param.nthreads)
    param.quiet=1;
#endif // ]

  if (verbose) {
    ///////////////////////////////////////////////////////////////////////////
#if defined (BG_PARAM_SKIP_SCAN) // [
    if (!param.skip_scan) {
#endif // ]
			if (bg_is_output_xml(&param))
        _FPRINTF(stdout," -->");

      _FPRINTF(stdout,"\n");
#if defined (BG_PARAM_SKIP_SCAN) // [
    }
#endif // ]

		if (bg_is_output_xml(&param))
      _FPRINTF(stdout,"<!-- ");

#if defined (BG_PARAM_THREADS) // [
    if (param.nthreads)
      _FPRINTF(stdout,"processing ...");
    else
      _FPRINTF(stdout,"analyzing ...");
#else // ] [
    _FPRINTF(stdout,"analyzing ...");
#endif // ]

		if (bg_is_output_xml(&param))
      _FPRINTF(stdout," -->");

    _FPRINTF(stdout,"\n");
    fflush(stdout);
  }

  bg_param_set_process(&param);

  if (bg_param_loop(&param,argv+optind)<0) {
    _DMESSAGE("processing");
    goto e_process;
  }

  if (verbose) {
    ///////////////////////////////////////////////////////////////////////////
		if (bg_is_output_xml(&param))
      _FPRINTF(stdout,"<!-- ");

    _FPRINTF(stdout,"done.");

		if (bg_is_output_xml(&param))
      _FPRINTF(stdout," -->");

    _FPRINTF(stdout,"\n");
    fflush(stdout);
  }

#if defined (BG_CLOCK) // [
  /////////////////////////////////////////////////////////////////////////////
  t2=clock();

  if (param.time&&!param.suppress.progress) {
		if (bg_is_output_xml(&param))
      _FPRINTF(stdout,"<!-- ");

    sec=(t2-t1)/CLOCKS_PER_SEC;

    if (sec<1.0)
      _FPRINTFV(stdout, "Duration: %.0f ms.",1000.0*sec);
    else if (sec<BG_MIN)
      _FPRINTFV(stdout, "Duration: %.3f sec.",sec);
    else if (sec<BG_HOUR)
      _FPRINTFV(stdout, "Duration: %.3f min.",sec/BG_MIN);
    else
      _FPRINTFV(stdout, "Duration: %.3f h.",sec/BG_HOUR);

		if (bg_is_output_xml(&param))
      _FPRINTF(stdout," -->");

    _FPRINTF(stdout,"\n");
    fflush(stdout);
  }
#endif // ]

  code=0;
//cleanup:
e_process:
e_count:
  bg_param_free_argumets(&param);
e_arguments:
  if (fpath)
    fclose(param.result.f);
e_file:
#if defined (_WIN32) // [
#if defined (BG_WIN32_CREATE_LOCALE) // [
  _free_locale(param.locale);
#endif // ]
#else // ] [
  if (locale)
    setlocale(LC_ALL,locale);
#endif // ]
#if defined (BG_PARAM_THREADS) // [
  bg_param_threads_destroy(&param.threads);
e_threads:
#endif // ]
e_help:
e_versions:
#if defined (HAVE_FF_DYNLOAD) // [
  ff_unload();
e_dynload:
#if defined (__linux__) && defined (__GNUC__) // [
e_libc:
#endif // ]
#endif // ]
  bg_param_destroy(&param);
e_arg:
  return code;
}

///////////////////////////////////////////////////////////////////////////////

#if defined (HAVE_FF_DYNLOAD) // [
#if defined (_WIN32) // [
#include <windows.h>
#else // ] [
//#include <linux/limits.h>
#include <sys/types.h>
#include <unistd.h>
#include <dlfcn.h>
#endif // ]
#if defined (__APPLE__) && defined (HAVE_LIBPROC) // [
#include <libproc.h>
#endif // ]

///////////////////////////////////////////////////////////////////////////////
#if 1 // [
#define DLOPEN_FLAG RTLD_LAZY
#else // ] [
#define DLOPEN_FLAG (RTLD_NOW|RTLD_GLOBAL)
#endif // ]

#if defined (_WIN32) // [
#define FFDLOPEN(path,flag) LoadLibraryW(path)
#define FFDLSYM(handle,symbol) GetProcAddress(handle,symbol)
#else // ] [
#define FFDLSYM(handle,symbol) dlsym(handle,symbol)
#define FFDLOPEN(path,flag) dlopen(path,flag)
#endif // ]

#define FF_DYNLOAD_FREE_LIST

///////////////////////////////////////////////////////////////////////////////
typedef struct ff_dynload_node ff_dynload_node_t;

struct ff_dynload_node {
  ff_dynload_node_t *prev;
#if defined (PBU_DEBUG) // [
  ffchar_t *id;
#endif // ]
#if defined (_WIN32) // [
  HANDLE hLib;
#else // ] [
  void *hLib;
#endif // ]
};

#if defined (FF_DYNLOAD_FREE_LIST) // [
static ff_dynload_node_t *ff_dynload_tail;

static void ff_dynload_list_append(ff_dynload_node_t *node,
    ffchar_t *id FFUNUSED)
{
  node->prev=ff_dynload_tail;
#if defined (PBU_DEBUG) // [
  node->id=id;
//FFVWRITELN("\"%s\"",id);
#endif // ]

  ff_dynload_tail=node;
}
#endif // ]

///////////////////////////////////////////////////////////////////////////////
#define FF_AVLOGLEVEL
static struct _ff_avutil {
  ff_dynload_node_t node;
  unsigned (*avutil_version)(void);
  AVFrame *(*av_frame_alloc)(void);
  void (*av_frame_free)(AVFrame **frame);
  int (*av_get_channel_layout_nb_channels)(uint64_t channel_layout);
#if 0 // [
  int64_t (*av_frame_get_best_effort_timestamp)(const AVFrame *frame);
  void (*av_frame_set_best_effort_timestamp)(AVFrame *frame, int64_t val);
  int (*av_log_get_level)(void);
#endif // ]
#if 0 // [
  const char *(*av_get_sample_fmt_name)(enum AVSampleFormat sample_fmt);
#if 0 // [
  void (*av_log)(void *avcl, int level, const char *fmt, ...)
      av_printf_format(3, 4);
#else // ] [
  void (*av_vlog)(void *avcl, int level, const char *fmt, va_list vl);
#endif // ]
#endif // ]
  int64_t (*av_rescale_q_rnd)(int64_t a, AVRational bq, AVRational cq,
      enum AVRounding) av_const;
  int64_t (*av_rescale_q)(int64_t a, AVRational bq, AVRational cq) av_const;
#if defined (FF_AV_RESCALE) // [
  int64_t (*av_rescale)(int64_t a, int64_t b, int64_t c);
#endif // ]
#if 0 // [
  void (*av_frame_set_channel_layout)(AVFrame *frame, int64_t val);
  int64_t (*av_frame_get_channel_layout)(const AVFrame *frame);
  void (*av_frame_set_channels)(AVFrame *frame, int val);
#if defined (FF_AV_FRAME_GET_CHANNELS) // [
  int (*av_frame_get_channels)(const AVFrame *frame);
#endif // ]
  void (*av_frame_set_sample_rate)(AVFrame *frame, int val);
  int  (*av_frame_get_sample_rate)(const AVFrame *frame);
  int (*av_samples_alloc)(uint8_t **audio_data, int *linesize,
      int nb_channels, int nb_samples, enum AVSampleFormat sample_fmt,
      int align);
  void (*av_free)(void *ptr);
  void (*av_freep)(void *ptr);
#endif // ]
  int (*av_dict_copy)(AVDictionary **dst, const AVDictionary *src, int flags);
  AVDictionaryEntry *(*av_dict_get)(const AVDictionary *m, const char *key,
      const AVDictionaryEntry *prev, int flags);
  int (*av_dict_set)(AVDictionary **pm, const char *key, const char *value,
      int flags);
  void (*av_dict_free)(AVDictionary **m);
  int (*av_frame_get_buffer)(AVFrame *frame, int align);
#if 0 // [
#if defined (FF_FILTER_CHANNELS) // [
  int (*av_get_channel_layout_channel_index)(uint64_t channel_layout,
      int index);
#endif // ]
#endif // ]
  ////
  void (*av_frame_unref)(AVFrame *frame);
  int (*av_opt_set_int)(void *obj, const char *name, int64_t val,
      int search_flags);
  int (*av_opt_set_sample_fmt)(void *obj, const char *name,
      enum AVSampleFormat fmt, int search_flags);
  int (*av_strerror)(int errnum, char *errbuf, size_t errbuf_size);
#if 0 // [
  AVAudioFifo *(*av_audio_fifo_alloc)(enum AVSampleFormat sample_fmt,
      int channels, int nb_samples);
  void (*av_audio_fifo_free)(AVAudioFifo *af);
  int (*av_audio_fifo_realloc)(AVAudioFifo *af, int nb_samples);
  int (*av_audio_fifo_write)(AVAudioFifo *af, void **data, int nb_samples);
  int (*av_audio_fifo_read)(AVAudioFifo *af, void **data, int nb_samples);
  int (*av_audio_fifo_size)(AVAudioFifo *af);
#endif // ]
  char *(*av_get_sample_fmt_string)(char *buf, int buf_size,
      enum AVSampleFormat sample_fmt);
  void (*av_get_channel_layout_string)(char *buf, int buf_size,
      int nb_channels, uint64_t channel_layout);
  const char *(*av_get_sample_fmt_name)(enum AVSampleFormat sample_fmt);
  unsigned (*av_int_list_length_for_size)(unsigned elsize, const void *list,
      uint64_t term);
  int (*av_opt_set_bin)(void *obj, const char *name, const uint8_t *val,
      int size, int search_flags);
  char *(*av_strdup)(const char *s);
  void (*av_vlog)(void *avcl, int level, const char *fmt, va_list vl);
  int64_t (*av_get_default_channel_layout)(int nb_channels);
#if 1 // [
  AVBufferRef *(*av_buffer_ref)(const AVBufferRef *buf);
#endif // ]
#if defined (FF_AVLOGLEVEL) // [
  int (*av_log_get_level)(void);
  void (*av_log_set_level)(int level);
#endif // ]
#if defined (HAVE_FF_DYNLOAD) // [
  int (*av_opt_set_chlayout)(void *obj, const char *name,
      const AVChannelLayout *layout, int search_flags);
  int (*av_channel_layout_compare)(const AVChannelLayout *chl,
      const AVChannelLayout *chl1);
  int (*av_channel_layout_describe)(const AVChannelLayout *channel_layout,
      char *buf, size_t buf_size);
  int (*av_channel_layout_check)(const AVChannelLayout *channel_layout);
  int (*av_opt_set)(void *obj, const char *name, const char *val,
      int search_flags);
#endif // ]
} avutil;

static int avutil_load_sym(void *p, const char *sym);

// [
unsigned avutil_version(void)
{
  if (avutil_load_sym(&avutil.avutil_version,__func__)<0)
    return 0u;

  return avutil.avutil_version();
}

AVFrame *av_frame_alloc(void)
{
  if (avutil_load_sym(&avutil.av_frame_alloc,__func__)<0)
    return NULL;

  return avutil.av_frame_alloc();
}

void av_frame_free(AVFrame **frame)
{
  if (avutil_load_sym(&avutil.av_frame_free,__func__)<0)
    return;

  avutil.av_frame_free(frame);
}

int av_get_channel_layout_nb_channels(uint64_t channel_layout)
{
  if (avutil_load_sym(&avutil.av_get_channel_layout_nb_channels,__func__)<0)
    return -1;

  return avutil.av_get_channel_layout_nb_channels(channel_layout);
}

#if 0 // [
int64_t av_frame_get_best_effort_timestamp(const AVFrame *frame)
{
  if (avutil_load_sym(&avutil.av_frame_get_best_effort_timestamp,__func__)<0)
    return -1;

  return avutil.av_frame_get_best_effort_timestamp(frame);
}

void av_frame_set_best_effort_timestamp(AVFrame *frame, int64_t val)
{
  if (avutil_load_sym(&avutil.av_frame_set_best_effort_timestamp,__func__)<0)
    return;

  avutil.av_frame_set_best_effort_timestamp(frame,val);
}

int av_log_get_level(void)
{
  if (avutil_load_sym(&avutil.av_log_get_level,__func__)<0)
    return -1;

  return avutil.av_log_get_level();
}
#endif // ]

#if defined (FF_AVLOGLEVEL) // [
int av_log_get_level(void)
{
  if (avutil_load_sym(&avutil.av_log_get_level,__func__)<0)
    return 0;

  return avutil.av_log_get_level();
}

void av_log_set_level(int level)
{
  if (avutil_load_sym(&avutil.av_log_set_level,__func__)<0)
    return;

  avutil.av_log_set_level(level);
}
#endif // ]

#if 0 // [
const char *av_get_sample_fmt_name(enum AVSampleFormat sample_fmt)
{
  if (avutil_load_sym(&avutil.av_get_sample_fmt_name,__func__)<0)
    return NULL;

  return avutil.av_get_sample_fmt_name(sample_fmt);
}

void av_log(void *avcl, int level, const char *fmt, ...)
{
  va_list ap;

  if (avutil_load_sym(&avutil.av_vlog,__func__)<0)
    return;

  va_start(ap,fmt);
  avutil.av_vlog(avcl,level,fmt,ap);
  va_end(ap);
}
#endif // ]

int64_t av_rescale_q_rnd(int64_t a, AVRational bq, AVRational cq,
    enum AVRounding rnd)
{
  if (avutil_load_sym(&avutil.av_rescale_q_rnd,__func__)<0)
    return -1;

  return avutil.av_rescale_q_rnd(a,bq,cq,rnd);
}

int64_t av_rescale_q(int64_t a, AVRational bq, AVRational cq)
{
  if (avutil_load_sym(&avutil.av_rescale_q,__func__)<0)
    return -1;

  return avutil.av_rescale_q(a,bq,cq);
}

#if defined (FF_AV_RESCALE) // [
int64_t av_rescale(int64_t a, int64_t b, int64_t c)
{
  if (avutil_load_sym(&avutil.av_rescale,__func__)<0)
    return -1ll;

  return avutil.av_rescale(a,b,c);
}
#endif // ]

#if 0 // [
void av_frame_set_channel_layout(AVFrame *frame, int64_t val)
{
  if (avutil_load_sym(&avutil.av_frame_set_channel_layout,__func__)<0)
    return;

  avutil.av_frame_set_channel_layout(frame,val);
}

int64_t av_frame_get_channel_layout(const AVFrame *frame)
{
  if (avutil_load_sym(&avutil.av_frame_get_channel_layout,__func__)<0)
    return -1;

  return avutil.av_frame_get_channel_layout(frame);
}

void av_frame_set_channels(AVFrame *frame, int val)
{
  if (avutil_load_sym(&avutil.av_frame_set_channels,__func__)<0)
    return;

  avutil.av_frame_set_channels(frame,val);
}

#if defined (FF_AV_FRAME_GET_CHANNELS) // [
int av_frame_get_channels(const AVFrame *frame)
{
  if (avutil_load_sym(&avutil.av_frame_get_channels,__func__)<0)
    return -1;

  return avutil.av_frame_get_channels(frame);
}
#endif // ]

void av_frame_set_sample_rate(AVFrame *frame, int val)
{
  if (avutil_load_sym(&avutil.av_frame_set_sample_rate,__func__)<0)
    return;

  avutil.av_frame_set_sample_rate(frame,val);
}

int  av_frame_get_sample_rate(const AVFrame *frame)
{
  if (avutil_load_sym(&avutil.av_frame_get_sample_rate,__func__)<0)
    return -1;

  return avutil.av_frame_get_sample_rate(frame);
}

int av_samples_alloc(uint8_t **audio_data, int *linesize,
    int nb_channels, int nb_samples, enum AVSampleFormat sample_fmt,
    int align)
{
  if (avutil_load_sym(&avutil.av_samples_alloc,__func__)<0)
    return -1;

  return avutil.av_samples_alloc(audio_data,linesize,nb_channels,
      nb_samples,sample_fmt,align);
}

void av_free(void *ptr)
{
  if (avutil_load_sym(&avutil.av_free,__func__)<0)
    return;

  avutil.av_free(ptr);
}

void av_freep(void *ptr)
{
  if (avutil_load_sym(&avutil.av_freep,__func__)<0)
    return;

  avutil.av_freep(ptr);
}
#endif // ]

int av_dict_copy(AVDictionary **dst, const AVDictionary *src, int	flags)
{
  if (avutil_load_sym(&avutil.av_dict_copy,__func__)<0)
    return -1;

  return avutil.av_dict_copy(dst,src,flags);
}

AVDictionaryEntry *av_dict_get(const AVDictionary *m, const char *key,
    const AVDictionaryEntry *prev, int flags)
{
  if (avutil_load_sym(&avutil.av_dict_get,__func__)<0)
    return NULL;

  return avutil.av_dict_get(m,key,prev,flags);
}

int av_dict_set(AVDictionary **pm, const char *key, const char *value,
    int flags)
{
  if (avutil_load_sym(&avutil.av_dict_set,__func__)<0)
    return -1;

  return avutil.av_dict_set(pm,key,value,flags);
}

void av_dict_free(AVDictionary **m)
{
  if (avutil_load_sym(&avutil.av_dict_free,__func__)<0)
    return;

  avutil.av_dict_free(m);
}

int av_frame_get_buffer(AVFrame *frame, int align)
{
  if (avutil_load_sym(&avutil.av_frame_get_buffer,__func__)<0)
    return -1;

  return avutil.av_frame_get_buffer(frame,align);
}

#if 0 // [
#if defined (FF_FILTER_CHANNELS) // [
int av_get_channel_layout_channel_index(uint64_t channel_layout,
    int index)
{
  if (avutil_load_sym(&avutil.av_get_channel_layout_channel_index,__func__)<0)
    return -1;

  return avutil.av_get_channel_layout_channel_index(channel_layout,index);
}
#endif // ]
#endif // ]

////
void av_frame_unref(AVFrame *frame)
{
  if (avutil_load_sym(&avutil.av_frame_unref,__func__)<0)
    return;

  avutil.av_frame_unref(frame);
}

int av_opt_set_int(void *obj, const char *name, int64_t val, int search_flags)
{
  if (avutil_load_sym(&avutil.av_opt_set_int,__func__)<0)
    return -1;

  return avutil.av_opt_set_int(obj,name,val,search_flags);
}

int av_opt_set_sample_fmt(void *obj, const char *name, enum AVSampleFormat fmt,
    int search_flags)
{
  if (avutil_load_sym(&avutil.av_opt_set_sample_fmt,__func__)<0)
    return -1;

  return avutil.av_opt_set_sample_fmt(obj,name,fmt,search_flags);
}

int av_strerror(int errnum, char *errbuf, size_t errbuf_size)
{
  if (avutil_load_sym(&avutil.av_strerror,__func__)<0)
    return -1;

  return avutil.av_strerror(errnum,errbuf,errbuf_size);
}

#if 0 // [
AVAudioFifo *av_audio_fifo_alloc(enum AVSampleFormat sample_fmt, int channels,
    int nb_samples)
{
  if (avutil_load_sym(&avutil.av_audio_fifo_alloc,__func__)<0)
    return NULL;

  return avutil.av_audio_fifo_alloc(sample_fmt,channels,nb_samples);
}

void av_audio_fifo_free(AVAudioFifo *af)
{
  if (avutil_load_sym(&avutil.av_audio_fifo_free,__func__)<0)
    return;

  avutil.av_audio_fifo_free(af);
}

int av_audio_fifo_realloc(AVAudioFifo *af, int nb_samples)
{
  if (avutil_load_sym(&avutil.av_audio_fifo_realloc,__func__)<0)
    return -1;

  return avutil.av_audio_fifo_realloc(af,nb_samples);
}

int av_audio_fifo_write(AVAudioFifo *af, void **data, int nb_samples)
{
  if (avutil_load_sym(&avutil.av_audio_fifo_write,__func__)<0)
    return -1;

  return avutil.av_audio_fifo_write(af,data,nb_samples);
}

int av_audio_fifo_read(AVAudioFifo *af, void **data, int nb_samples)
{
  if (avutil_load_sym(&avutil.av_audio_fifo_read,__func__)<0)
    return -1;

  return avutil.av_audio_fifo_read(af,data,nb_samples);
}

int av_audio_fifo_size(AVAudioFifo *af)
{
  if (avutil_load_sym(&avutil.av_audio_fifo_size,__func__)<0)
    return -1;

  return avutil.av_audio_fifo_size(af);
}
#endif // ]

#if defined (HAVE_FF_DYNLOAD) // [
int av_opt_set(void *obj, const char *name, const char *val,
    int search_flags)
{
  if (avutil_load_sym(&avutil.av_opt_set,__func__)<0)
    return -1;

  return avutil.av_opt_set(obj, name, val, search_flags);
}

int av_channel_layout_check(const AVChannelLayout *channel_layout)
{
  if (avutil_load_sym(&avutil.av_channel_layout_check,__func__)<0)
    return -1;

  return avutil.av_channel_layout_check(channel_layout);
}

int av_channel_layout_describe(const AVChannelLayout *channel_layout,
    char *buf, size_t buf_size)
{
  if (avutil_load_sym(&avutil.av_channel_layout_describe,__func__)<0)
    return -1;

  return avutil.av_channel_layout_describe(channel_layout, buf, buf_size);
}

int av_opt_set_chlayout(void *obj, const char *name,
    const AVChannelLayout *layout, int search_flags)
{
  if (avutil_load_sym(&avutil.av_opt_set_chlayout,__func__)<0)
    return -1;

  return avutil.av_opt_set_chlayout(obj, name, layout, search_flags);
}

int av_channel_layout_compare(const AVChannelLayout *chl,
    const AVChannelLayout *chl1)
{
  if (avutil_load_sym(&avutil.av_channel_layout_compare,__func__)<0)
    return -1;

  return avutil.av_channel_layout_compare(chl,chl1);
}

char *av_get_sample_fmt_string(char *buf, int buf_size,
    enum AVSampleFormat sample_fmt)
{
  if (avutil_load_sym(&avutil.av_get_sample_fmt_string,__func__)<0)
    return NULL;

  return avutil.av_get_sample_fmt_string(buf,buf_size,sample_fmt);
}
#endif // ]

void av_get_channel_layout_string(char *buf, int buf_size, int nb_channels,
    uint64_t channel_layout)
{
  if (avutil_load_sym(&avutil.av_get_channel_layout_string,__func__)<0)
    return;

  avutil.av_get_channel_layout_string(buf,buf_size,nb_channels,channel_layout);
}

const char *av_get_sample_fmt_name(enum AVSampleFormat sample_fmt)
{
  if (avutil_load_sym(&avutil.av_get_sample_fmt_name,__func__)<0)
    return NULL;

  return avutil.av_get_sample_fmt_name(sample_fmt);
}

unsigned av_int_list_length_for_size(unsigned elsize, const void *list,
    uint64_t term)
{
  if (avutil_load_sym(&avutil.av_int_list_length_for_size,__func__)<0)
    return 0u;

  return avutil.av_int_list_length_for_size(elsize,list,term);
}

int av_opt_set_bin(void *obj, const char *name, const uint8_t *val, int size,
    int search_flags)
{
  if (avutil_load_sym(&avutil.av_opt_set_bin,__func__)<0)
    return -1;

  return avutil.av_opt_set_bin(obj,name,val,size,search_flags);
}

char *av_strdup(const char *s)
{
  if (avutil_load_sym(&avutil.av_strdup,__func__)<0)
    return NULL;

  return avutil.av_strdup(s);
}

void av_log(void *avcl, int level, const char *fmt, ...)
{
  va_list ap;

  if (avutil_load_sym(&avutil.av_vlog,"av_vlog")<0)
    return;

  va_start(ap,fmt);
  avutil.av_vlog(avcl,level,fmt,ap);
  va_end(ap);
}

int64_t av_get_default_channel_layout(int nb_channels)
{
  if (avutil_load_sym(&avutil.av_get_default_channel_layout,__func__)<0)
    return 0ll;

  return avutil.av_get_default_channel_layout(nb_channels);
}

#if 1 // [
AVBufferRef *av_buffer_ref(const AVBufferRef *buf)
{
  if (avutil_load_sym(&avutil.av_buffer_ref,__func__)<0)
    return NULL;

  return avutil.av_buffer_ref(buf);
}
#endif // ]
// ]

///////////////////////////////////////////////////////////////////////////////
static struct _ff_avcodec {
  ff_dynload_node_t node;
  unsigned (*avcodec_version)(void);
#if (LIBAVCODEC_VERSION_MAJOR<58) // [
  void (*avcodec_register_all)(void);
	AVCodec *(*av_codec_next)(const AVCodec *c);
	AVOutputFormat *(*av_oformat_next)(const AVOutputFormat *f);
#endif // ]
  FF_CONST AVCodec *(*avcodec_find_decoder)(enum AVCodecID id);
  FF_CONST AVCodec *(*avcodec_find_decoder_by_name)(const char *name);
  FF_CONST AVCodec *(*avcodec_find_encoder)(enum AVCodecID id);
  int (*avcodec_open2)(AVCodecContext *avctx, const AVCodec *codec,
      AVDictionary **options);
#if 0 // [
  void (*av_init_packet)(AVPacket *pkt);
#endif // ]
  int (*avcodec_send_packet)(AVCodecContext *avctx, const AVPacket *avpkt);
  int (*avcodec_receive_frame)(AVCodecContext *avctx, AVFrame *frame);
  int (*avcodec_send_frame)(AVCodecContext *avctx, const AVFrame *frame);
  int (*avcodec_receive_packet)(AVCodecContext *avctx, AVPacket *avpkt);
#if 0 // [
  int (*avcodec_decode_audio4)(AVCodecContext *avctx, AVFrame *frame,
      int *got_frame_ptr, const AVPacket *avpkt);
  int (*avcodec_encode_audio2)(AVCodecContext *avctx, AVPacket *avpkt,
      const AVFrame *frame, int *got_packet_ptr);
  int (*avcodec_decode_video2)(AVCodecContext *avctx, AVFrame *picture,
      int *got_picture_ptr, const AVPacket *avpkt);
#endif // ]
  void (*av_packet_unref)(AVPacket *pkt);
  int (*avcodec_close)(AVCodecContext *avctx);
#if 0 // [
  int (*avcodec_copy_context)(AVCodecContext *dest, const AVCodecContext *src);
#endif // ]
  void (*av_packet_rescale_ts)(AVPacket *pkt, AVRational tb_src,
      AVRational tb_dst);
  ////
  AVCodecContext *(*avcodec_alloc_context3)(const AVCodec *codec);
  void (*avcodec_free_context)(AVCodecContext **avctx);
  AVPacket *(*av_packet_alloc)(void);
  void (*av_packet_free)(AVPacket **pkt);
  AVCodecParameters *(*avcodec_parameters_alloc)(void);
  void (*avcodec_parameters_free)(AVCodecParameters **par);
  int (*avcodec_parameters_copy)(AVCodecParameters *dst,
      const AVCodecParameters *src);
  int (*avcodec_parameters_to_context)(AVCodecContext *codec,
      const AVCodecParameters *par);
  int (*avcodec_parameters_from_context)(AVCodecParameters *par,
      const AVCodecContext *codec);
  const char *(*avcodec_get_name)(enum AVCodecID id);
  void (*avcodec_flush_buffers)(AVCodecContext *avctx);
  FF_CONST AVCodec *(*avcodec_find_encoder_by_name)(const char *name);
  const AVCodec *(*av_codec_iterate)(void **opaque);
#if 0 // [
  void (*av_init_packet)(AVPacket *pkt);
#endif // ]
  int (*av_packet_copy_props)(AVPacket *dst, const AVPacket *src);
} avcodec;

static int avcodec_load_sym(void *p, const char *sym);

// [
unsigned avcodec_version(void)
{
#if 0 // [
  if (avcodec_load_sym(&avcodec.avcodec_version,__func__)<0)
    return 0u;

  return avcodec.avcodec_version();
#else // ] [
  unsigned version=0u;

  if (avcodec_load_sym(&avcodec.avcodec_version,__func__)<0)
    goto exit;

  version=avcodec.avcodec_version();
exit:
  return version;
#endif // ]
}

#if (LIBAVCODEC_VERSION_MAJOR<58) // [
void avcodec_register_all(void)
{
  if (avcodec_load_sym(&avcodec.avcodec_register_all,__func__)<0)
    return;

  avcodec.avcodec_register_all();
}

AVCodec *av_codec_next(const AVCodec *c)
{
  if (avcodec_load_sym(&avcodec.av_codec_next,__func__)<0)
    return NULL;

  return avcodec.av_codec_next(c);
}

AVOutputFormat *av_oformat_next(const AVOutputFormat *f)
{
  if (avcodec_load_sym(&avcodec.av_oformat_next,__func__)<0)
    return NULL;

  return avcodec.av_oformat_next(f);
}
#endif // ]

FF_CONST AVCodec *avcodec_find_decoder(enum AVCodecID id)
{
  if (avcodec_load_sym(&avcodec.avcodec_find_decoder,__func__)<0)
    return NULL;

  return avcodec.avcodec_find_decoder(id);
}

FF_CONST AVCodec *avcodec_find_decoder_by_name(const char *name)
{
  if (avcodec_load_sym(&avcodec.avcodec_find_decoder_by_name,__func__)<0)
    return NULL;

  return avcodec.avcodec_find_decoder_by_name(name);
}

FF_CONST AVCodec *avcodec_find_encoder(enum AVCodecID id)
{
  if (avcodec_load_sym(&avcodec.avcodec_find_encoder,__func__)<0)
    return NULL;

  return avcodec.avcodec_find_encoder(id);
}

int avcodec_open2(AVCodecContext *avctx, const AVCodec *codec,
    AVDictionary **options)
{
  if (avcodec_load_sym(&avcodec.avcodec_open2,__func__)<0)
    return -1;

  return avcodec.avcodec_open2(avctx,codec,options);
}

#if 0 // [
void av_init_packet(AVPacket *pkt)
{
  if (avcodec_load_sym(&avcodec.av_init_packet,__func__)<0)
    return;

  avcodec.av_init_packet(pkt);
}
#endif // ]

int avcodec_send_packet(AVCodecContext *avctx, const AVPacket *avpkt)
{
  if (avcodec_load_sym(&avcodec.avcodec_send_packet,__func__)<0)
    return -1;

  return avcodec.avcodec_send_packet(avctx,avpkt);
}

int avcodec_receive_frame(AVCodecContext *avctx, AVFrame *frame)
{
  if (avcodec_load_sym(&avcodec.avcodec_receive_frame,__func__)<0)
    return -1;

  return avcodec.avcodec_receive_frame(avctx,frame);
}

int avcodec_send_frame(AVCodecContext *avctx, const AVFrame *frame)
{
  if (avcodec_load_sym(&avcodec.avcodec_send_frame,__func__)<0)
    return -1;

  return avcodec.avcodec_send_frame(avctx,frame);
}

int avcodec_receive_packet(AVCodecContext *avctx, AVPacket *avpkt)
{
  if (avcodec_load_sym(&avcodec.avcodec_receive_packet,__func__)<0)
    return -1;

  return avcodec.avcodec_receive_packet(avctx,avpkt);
}

#if 0 // [
int avcodec_decode_audio4(AVCodecContext *avctx, AVFrame *frame,
    int *got_frame_ptr, const AVPacket *avpkt)
{
  if (avcodec_load_sym(&avcodec.avcodec_decode_audio4,__func__)<0)
    return -1;

  return avcodec.avcodec_decode_audio4(avctx,frame,got_frame_ptr,avpkt);
}

int avcodec_encode_audio2(AVCodecContext *avctx, AVPacket *avpkt,
    const AVFrame *frame, int *got_packet_ptr)
{
  if (avcodec_load_sym(&avcodec.avcodec_encode_audio2,__func__)<0)
    return -1;

  return avcodec.avcodec_encode_audio2(avctx,avpkt,frame,got_packet_ptr);
}

int avcodec_decode_video2(AVCodecContext *avctx, AVFrame *picture,
    int *got_picture_ptr,const AVPacket *avpkt)
{
  if (avcodec_load_sym(&avcodec.avcodec_decode_video2,__func__)<0)
    return -1;

  return avcodec.avcodec_decode_video2(avctx,picture,got_picture_ptr,avpkt);
}
#endif // ]

void av_packet_unref(AVPacket *pkt)
{
  if (avcodec_load_sym(&avcodec.av_packet_unref,__func__)<0)
    return;

  avcodec.av_packet_unref(pkt);
}

int avcodec_close(AVCodecContext *avctx)
{
  if (avcodec_load_sym(&avcodec.avcodec_close,__func__)<0)
    return -1;

  return avcodec.avcodec_close(avctx);
}

#if 0 // [
int avcodec_copy_context(AVCodecContext *dest, const AVCodecContext *src)
{
  if (avcodec_load_sym(&avcodec.avcodec_copy_context,__func__)<0)
    return -1;

  return avcodec.avcodec_copy_context(dest,src);
}
#endif // ]

void av_packet_rescale_ts(AVPacket *pkt, AVRational tb_src,
    AVRational tb_dst)
{
  if (avcodec_load_sym(&avcodec.av_packet_rescale_ts,__func__)<0)
    return;

  avcodec.av_packet_rescale_ts(pkt,tb_src,tb_dst);
}

////
AVCodecContext *avcodec_alloc_context3(const AVCodec *codec)
{
  if (avcodec_load_sym(&avcodec.avcodec_alloc_context3,__func__)<0)
    return NULL;

  return avcodec.avcodec_alloc_context3(codec);
}

void avcodec_free_context(AVCodecContext **avctx)
{
  if (avcodec_load_sym(&avcodec.avcodec_free_context,__func__)<0)
    return;

  avcodec.avcodec_free_context(avctx);
}

AVPacket *av_packet_alloc(void)
{
  if (avcodec_load_sym(&avcodec.av_packet_alloc,__func__)<0)
    return NULL;

  return avcodec.av_packet_alloc();
}

void av_packet_free(AVPacket **pkt)
{
  if (avcodec_load_sym(&avcodec.av_packet_free,__func__)<0)
    return;

  avcodec.av_packet_free(pkt);
}

AVCodecParameters *avcodec_parameters_alloc(void)
{
  if (avcodec_load_sym(&avcodec.avcodec_parameters_alloc,__func__)<0)
    return NULL;

  return avcodec.avcodec_parameters_alloc();
}

void avcodec_parameters_free(AVCodecParameters **par)
{
  if (avcodec_load_sym(&avcodec.avcodec_parameters_free,__func__)<0)
    return;

  avcodec.avcodec_parameters_free(par);
}

int avcodec_parameters_copy(AVCodecParameters *dst,
    const AVCodecParameters *src)
{
  if (avcodec_load_sym(&avcodec.avcodec_parameters_copy,__func__)<0)
    return -1;

  return avcodec.avcodec_parameters_copy(dst,src);
}

int avcodec_parameters_to_context(AVCodecContext *codec,
    const AVCodecParameters *par)
{
  if (avcodec_load_sym(&avcodec.avcodec_parameters_to_context,__func__)<0)
    return -1;

  return avcodec.avcodec_parameters_to_context(codec,par);
}

int avcodec_parameters_from_context(AVCodecParameters *par,
    const AVCodecContext *codec)
{
  if (avcodec_load_sym(&avcodec.avcodec_parameters_from_context,__func__)<0)
    return -1;

  return avcodec.avcodec_parameters_from_context(par,codec);
}

const char *avcodec_get_name(enum AVCodecID id)
{
  if (avcodec_load_sym(&avcodec.avcodec_get_name,__func__)<0)
    return NULL;

  return avcodec.avcodec_get_name(id);
}

void avcodec_flush_buffers(AVCodecContext *avctx)
{
  if (avcodec_load_sym(&avcodec.avcodec_flush_buffers,__func__)<0)
    return;

  avcodec.avcodec_flush_buffers(avctx);
}

FF_CONST AVCodec *avcodec_find_encoder_by_name(const char *name)
{
  if (avcodec_load_sym(&avcodec.avcodec_find_encoder_by_name,__func__)<0)
    return NULL;

  return avcodec.avcodec_find_encoder_by_name(name);
}

const AVCodec *av_codec_iterate(void **opaque)
{
  if (avcodec_load_sym(&avcodec.av_codec_iterate,__func__)<0)
    return NULL;

  return avcodec.av_codec_iterate(opaque);
}

#if 0 // [
void av_init_packet(AVPacket *pkt)
{
  if (avcodec_load_sym(&avcodec.av_init_packet,__func__)<0)
    return;

  avcodec.av_init_packet(pkt);
}
#endif // ]

int av_packet_copy_props(AVPacket *dst, const AVPacket *src)
{
  if (avcodec_load_sym(&avcodec.av_packet_copy_props,__func__)<0)
    return -1;

  return avcodec.av_packet_copy_props(dst,src);
}
// ]

///////////////////////////////////////////////////////////////////////////////
static struct _ff_avformat {
  ff_dynload_node_t node;
  unsigned (*avformat_version)(void);
#if (LIBAVFORMAT_VERSION_MAJOR<58) // [
  void (*av_register_all)(void);
#endif // ]
  int (*avformat_open_input)(AVFormatContext **ps, const char *filename,
      FF_CONST AVInputFormat *fmt, AVDictionary **options);
  int (*avformat_find_stream_info)(AVFormatContext *ic,
      AVDictionary **options);
  int (*av_read_frame)(AVFormatContext *s, AVPacket *pkt);
  void (*avformat_close_input)(AVFormatContext **s);
  int (*avformat_alloc_output_context2)(AVFormatContext **ctx,
      FF_CONST AVOutputFormat *oformat, const char *format_name,
      const char *filename);
  void (*avformat_free_context)(AVFormatContext *s);
  AVStream *(*avformat_new_stream)(AVFormatContext *s, const AVCodec *c);
  int (*avio_open)(AVIOContext **s, const char *url, int flags);
  int (*avio_closep)(AVIOContext **s);
  int (*avformat_write_header)(AVFormatContext *s, AVDictionary **options);
  int (*av_interleaved_write_frame)(AVFormatContext *s, AVPacket *pkt);
  int (*av_write_trailer)(AVFormatContext *s);
#if 0 // [
  int (*av_find_default_stream_index)(AVFormatContext *s);
#endif // ]
  void (*av_dump_format)(AVFormatContext *ic, int index, const char *url,
      int is_output);
  int (*avformat_seek_file)(AVFormatContext *s, int stream_index,
      int64_t min_ts, int64_t ts, int64_t max_ts, int flags);
  int (*av_seek_frame)(AVFormatContext *s, int stream_index,
      int64_t timestamp, int flags);
  int (*avformat_flush)(AVFormatContext *s);
  const AVOutputFormat *(*av_muxer_iterate)(void **opaque);
  int (*avformat_query_codec)(const AVOutputFormat *ofmt,
      enum AVCodecID codec_id, int std_compliance);
} avformat;

static int avformat_load_sym(void *p, const char *sym);

// [
unsigned avformat_version(void)
{
  if (avformat_load_sym(&avformat.avformat_version,__func__)<0)
    return 0u;

  return avformat.avformat_version();
}

#if (LIBAVFORMAT_VERSION_MAJOR<58) // [
void av_register_all(void)
{
  if (avformat_load_sym(&avformat.av_register_all,__func__)<0)
    return;

  avformat.av_register_all();
}
#endif // ]

int avformat_open_input(AVFormatContext **ps, const char *filename,
    FF_CONST AVInputFormat *fmt, AVDictionary **options)
{
  if (avformat_load_sym(&avformat.avformat_open_input,__func__)<0)
    return -1;

  return avformat.avformat_open_input(ps,filename,fmt,options);
}

int avformat_find_stream_info(AVFormatContext *ic, AVDictionary **options)
{
  if (avformat_load_sym(&avformat.avformat_find_stream_info,__func__)<0)
    return -1;

  return avformat.avformat_find_stream_info(ic,options);
}

int av_read_frame(AVFormatContext *s, AVPacket *pkt)
{
  if (avformat_load_sym(&avformat.av_read_frame,__func__)<0)
    return -1;

  return avformat.av_read_frame(s,pkt);
}

void avformat_close_input(AVFormatContext **s)
{
  if (avformat_load_sym(&avformat.avformat_close_input,__func__)<0)
    return;

  avformat.avformat_close_input(s);
}

int avformat_alloc_output_context2(AVFormatContext **ctx,
    FF_CONST AVOutputFormat *oformat, const char *format_name,
    const char *filename)
{
  if (avformat_load_sym(&avformat.avformat_alloc_output_context2,__func__)<0)
    return -1;

  return avformat.avformat_alloc_output_context2(ctx,oformat,format_name,
      filename);
}

void avformat_free_context(AVFormatContext *s)
{
  if (avformat_load_sym(&avformat.avformat_free_context,__func__)<0)
    return;

  avformat.avformat_free_context(s);
}

AVStream *avformat_new_stream(AVFormatContext *s, const AVCodec *c)
{
  if (avformat_load_sym(&avformat.avformat_new_stream,__func__)<0)
    return NULL;

  return avformat.avformat_new_stream(s,c);
}

int avio_open(AVIOContext **s, const char *url, int flags)
{
  if (avformat_load_sym(&avformat.avio_open,__func__)<0)
    return -1;

  return avformat.avio_open(s,url,flags);
}

int avio_closep(AVIOContext **s)
{
  if (avformat_load_sym(&avformat.avio_closep,__func__)<0)
    return -1;

  return avformat.avio_closep(s);
}

int avformat_write_header(AVFormatContext *s, AVDictionary **options)
{
  if (avformat_load_sym(&avformat.avformat_write_header,__func__)<0)
    return -1;

  return avformat.avformat_write_header(s,options);
}

int av_interleaved_write_frame(AVFormatContext *s, AVPacket *pkt)
{
  if (avformat_load_sym(&avformat.av_interleaved_write_frame,__func__)<0)
    return -1;

  return avformat.av_interleaved_write_frame(s,pkt);
}

int av_write_trailer(AVFormatContext *s)
{
  if (avformat_load_sym(&avformat.av_write_trailer,__func__)<0)
    return -1;

  return avformat.av_write_trailer(s);
}

#if 0 // [
int av_find_default_stream_index(AVFormatContext *s)
{
  if (avformat_load_sym(&avformat.av_find_default_stream_index,__func__)<0)
    return -1;

  return avformat.av_find_default_stream_index(s);
}
#endif // ]

void av_dump_format(AVFormatContext *ic, int index, const char *url,
    int is_output)
{
  if (avformat_load_sym(&avformat.av_dump_format,__func__)<0)
    return;

  avformat.av_dump_format(ic,index,url,is_output);
}

int avformat_seek_file(AVFormatContext *s, int stream_index,
    int64_t min_ts, int64_t ts, int64_t max_ts, int flags)
{
  if (avformat_load_sym(&avformat.avformat_seek_file,__func__)<0)
    return -1;

  return avformat.avformat_seek_file(s,stream_index,min_ts,ts,max_ts,flags);
}

int av_seek_frame(AVFormatContext *s, int stream_index, int64_t timestamp,
    int flags)
{
  if (avformat_load_sym(&avformat.av_seek_frame,__func__)<0)
    return -1;

  return avformat.av_seek_frame(s,stream_index,timestamp,flags);
}

int avformat_flush(AVFormatContext *s)
{
  if (avformat_load_sym(&avformat.avformat_flush,__func__)<0)
    return -1;

  return avformat.avformat_flush(s);
}

const AVOutputFormat *av_muxer_iterate(void **opaque)
{
  if (avformat_load_sym(&avformat.av_muxer_iterate,__func__)<0)
    return NULL;

  return avformat.av_muxer_iterate(opaque);
}

int avformat_query_codec(const AVOutputFormat *ofmt, enum AVCodecID codec_id,
    int std_compliance)
{
  if (avformat_load_sym(&avformat.avformat_query_codec,__func__)<0)
    return 0;

  return avformat.avformat_query_codec(ofmt,codec_id,std_compliance);
}
// ]

///////////////////////////////////////////////////////////////////////////////
static struct _ff_swresample {
  ff_dynload_node_t node;
  unsigned (*swresample_version)(void);
  ////
  struct SwrContext *(*swr_alloc)(void);
  void (*swr_free)(struct SwrContext **s);
  int (*swr_init)(struct SwrContext *s);
  void (*swr_close)(struct SwrContext *s);
  struct SwrContext *(*swr_alloc_set_opts)(struct SwrContext *s,
      int64_t out_ch_layout, enum AVSampleFormat out_sample_fmt,
      int out_sample_rate, int64_t in_ch_layout,
      enum AVSampleFormat in_sample_fmt, int in_sample_rate,
      int log_offset, void *log_ctx);
  int (*swr_convert_frame)(SwrContext *swr, AVFrame *output,
      const AVFrame *input);
} swresample;

static int swresample_load_sym(void *p, const char *sym);

unsigned swresample_version(void)
{
  if (swresample_load_sym(&swresample.swresample_version,__func__)<0)
    return 0u;

  return swresample.swresample_version();
}

////
struct SwrContext *swr_alloc(void)
{
  if (swresample_load_sym(&swresample.swr_alloc,__func__)<0)
    return NULL;

  return swresample.swr_alloc();
}

void swr_free(struct SwrContext **s)
{
  if (swresample_load_sym(&swresample.swr_free,__func__)<0)
    return;

  swresample.swr_free(s);
}

int swr_init(struct SwrContext *s)
{
  if (swresample_load_sym(&swresample.swr_init,__func__)<0)
    return -1;

  return swresample.swr_init(s);
}

void swr_close(struct SwrContext *s)
{
  if (swresample_load_sym(&swresample.swr_close,__func__)<0)
    return;

  swresample.swr_close(s);
}

struct SwrContext *swr_alloc_set_opts(struct SwrContext *s,
    int64_t out_ch_layout, enum AVSampleFormat out_sample_fmt,
    int out_sample_rate, int64_t in_ch_layout,
    enum AVSampleFormat in_sample_fmt, int in_sample_rate,
    int log_offset, void *log_ctx)
{
  if (swresample_load_sym(&swresample.swr_alloc_set_opts,__func__)<0)
    return NULL;

  return swresample.swr_alloc_set_opts(s,out_ch_layout,out_sample_fmt,
      out_sample_rate,in_ch_layout,in_sample_fmt,in_sample_rate,log_offset,
      log_ctx);
}

int swr_convert_frame(SwrContext *swr, AVFrame *output, const AVFrame *input)
{
  if (swresample_load_sym(&swresample.swr_convert_frame,__func__)<0)
    return -1;

  return swresample.swr_convert_frame(swr,output,input);
}

///////////////////////////////////////////////////////////////////////////////
static struct _ff_swscale {
  ff_dynload_node_t node;
  unsigned (*swscale_version)(void);
} swscale;

static int swscale_load_sym(void *p, const char *sym);

unsigned swscale_version(void)
{
  if (swscale_load_sym(&swscale.swscale_version,__func__)<0)
    return 0u;

  return swscale.swscale_version();
}

///////////////////////////////////////////////////////////////////////////////
static struct _ff_postproc {
  ff_dynload_node_t node;
  unsigned (*postproc_version)(void);
} postproc;

static int postproc_load_sym(void *p, const char *sym) FFUNUSED;

unsigned postproc_version(void)
{
  if (postproc_load_sym(&postproc.postproc_version,__func__)<0)
    return 0u;

  return postproc.postproc_version();
}

///////////////////////////////////////////////////////////////////////////////
static struct _ff_avfilter {
  ff_dynload_node_t node;
  unsigned (*avfilter_version)(void);
#if (LIBAVFILTER_VERSION_MAJOR<7) // [
  void (*avfilter_register_all)(void);
#endif // ]
  AVFilterGraph *(*avfilter_graph_alloc)(void);
#if (LIBAVFILTER_VERSION_MAJOR<7) // [
  AVFilter *(*avfilter_get_by_name)(const char *name);
#else // ] [
  const AVFilter *(*avfilter_get_by_name)(const char *name);
#endif // ]
  int (*avfilter_graph_create_filter)(AVFilterContext **filt_ctx,
      const AVFilter *filt, const char *name, const char *args,
      void *opaque, AVFilterGraph *graph_ctx);
  int (*avfilter_graph_parse_ptr)(AVFilterGraph *graph, const char *filters,
      AVFilterInOut **inputs, AVFilterInOut **outputs, void *log_ctx);
  AVFilterInOut *(*avfilter_inout_alloc)(void);
  int (*avfilter_graph_config)(AVFilterGraph *graphctx, void *log_ctx);
  void (*avfilter_inout_free)(AVFilterInOut **inout);
  void (*avfilter_graph_free)(AVFilterGraph **graph);
  int (*av_buffersrc_add_frame)(AVFilterContext *ctx, AVFrame *frame);
  int (*av_buffersink_get_frame)(AVFilterContext *ctx, AVFrame *frame);
  void (*av_buffersink_set_frame_size)(AVFilterContext *ctx,
      unsigned frame_size);
} avfilter;

static int avfilter_load_sym(void *p, const char *sym);

// [
unsigned avfilter_version(void)
{
  if (avfilter_load_sym(&avfilter.avfilter_version,__func__)<0)
    return 0u;

  return avfilter.avfilter_version();
}

#if (LIBAVFILTER_VERSION_MAJOR<7) // [
void avfilter_register_all(void)
{
  if (avfilter_load_sym(&avfilter.avfilter_register_all,__func__)<0)
    return;

  avfilter.avfilter_register_all();
}
#endif // ]

AVFilterGraph *avfilter_graph_alloc(void)
{
  if (avfilter_load_sym(&avfilter.avfilter_graph_alloc,__func__)<0)
    return NULL;

  return avfilter.avfilter_graph_alloc();
}

#if (LIBAVFILTER_VERSION_MAJOR<7) // [
AVFilter *avfilter_get_by_name(const char *name)
#else // ] [
const AVFilter *avfilter_get_by_name(const char *name)
#endif // ]
{
  if (avfilter_load_sym(&avfilter.avfilter_get_by_name,__func__)<0)
    return NULL;

  return avfilter.avfilter_get_by_name(name);
}

int avfilter_graph_create_filter(AVFilterContext **filt_ctx,
    const AVFilter *filt, const char *name, const char *args,
    void *opaque, AVFilterGraph *graph_ctx)
{
  if (avfilter_load_sym(&avfilter.avfilter_graph_create_filter,__func__)<0)
    return -1;

  return avfilter.avfilter_graph_create_filter(filt_ctx,filt,name,args,
      opaque,graph_ctx);
}

int avfilter_graph_parse_ptr(AVFilterGraph *graph, const char *filters,
    AVFilterInOut **inputs, AVFilterInOut **outputs, void *log_ctx)
{
  if (avfilter_load_sym(&avfilter.avfilter_graph_parse_ptr,__func__)<0)
    return -1;

  return avfilter.avfilter_graph_parse_ptr(graph,filters,inputs,outputs,
      log_ctx);
}

AVFilterInOut *avfilter_inout_alloc(void)
{
  if (avfilter_load_sym(&avfilter.avfilter_inout_alloc,__func__)<0)
    return NULL;

  return avfilter.avfilter_inout_alloc();
}

int avfilter_graph_config(AVFilterGraph *graphctx, void *log_ctx)
{
  if (avfilter_load_sym(&avfilter.avfilter_graph_config,__func__)<0)
    return -1;

  return avfilter.avfilter_graph_config(graphctx,log_ctx);
}

void avfilter_inout_free(AVFilterInOut **inout)
{
  if (avfilter_load_sym(&avfilter.avfilter_inout_free,__func__)<0)
    return;

  avfilter.avfilter_inout_free(inout);
}

void avfilter_graph_free(AVFilterGraph **graph)
{
  if (avfilter_load_sym(&avfilter.avfilter_graph_free,__func__)<0)
    return;

  avfilter.avfilter_graph_free(graph);
}

int av_buffersrc_add_frame(AVFilterContext *ctx, AVFrame *frame)
{
  if (avfilter_load_sym(&avfilter.av_buffersrc_add_frame,__func__)<0)
    return -1;

  return avfilter.av_buffersrc_add_frame(ctx,frame);
}

int av_buffersink_get_frame(AVFilterContext *ctx, AVFrame *frame)
{
  if (avfilter_load_sym(&avfilter.av_buffersink_get_frame,__func__)<0)
    return -1;

  return avfilter.av_buffersink_get_frame(ctx,frame);
}


void av_buffersink_set_frame_size(AVFilterContext *ctx, unsigned frame_size)
{
  if (avfilter_load_sym(&avfilter.av_buffersink_set_frame_size,__func__)<0)
    return;

  avfilter.av_buffersink_set_frame_size(ctx,frame_size);
}
// ]

///////////////////////////////////////////////////////////////////////////////
static ffchar_t path[PATH_MAX],*pp=path;

#if defined (_WIN32) // [
static int load(HMODULE hLib, const char *sym, void *p)
#else // ] [
static int load(void *hLib, const char *sym, void *p)
#endif // ]
{
  void **fp=p;

  if (!*fp) {
    *fp=FFDLSYM(hLib,sym);

    if (!*fp) {
      _DMESSAGEV("loading %s",sym);
      return -1;
    }
  }

  return 0;
}

///////////////////////////////////////////////////////////////////////////////
static int avutil_load(void);
static int swresample_load(void);
static int avcodec_load(void);
static int avformat_load(void);
static int swscale_load(void);
static int postproc_load(void);
static int avfilter_load(void);

///////////////////////////////////////////////////////////////////////////////
static int avutil_load(void)
{
  static const ffchar_t AVUTIL[]=FF_AVUTIL;

  enum {
    SIZE_PATH=(sizeof path)/(sizeof path[0]),
    SIZE_AVUTIL=(sizeof AVUTIL)/(sizeof AVUTIL[0]),
  };

  if (!avutil.node.hLib) {
    if ((path+SIZE_PATH)<=(pp+SIZE_AVUTIL)) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",AVUTIL);
#else // ] [
      _DMESSAGEV("loading %s",AVUTIL);
#endif // ]
      return -1;
    }

    FFSTRCPY(pp,AVUTIL);
    avutil.node.hLib=FFDLOPEN(path,DLOPEN_FLAG);

    if (!avutil.node.hLib&&!(avutil.node.hLib=FFDLOPEN(AVUTIL,DLOPEN_FLAG))) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",path);
#else // ] [
      _DMESSAGEV("loading %s",path);
#endif // ]
      return -1;
    }

#if defined (FF_DYNLOAD_FREE_LIST) // [
    ff_dynload_list_append(&avutil.node,FFL("avutil"));
#endif // ]
  }

  return 0;
}

static int avcodec_load(void)
{
  static const ffchar_t AVCODEC[]=FF_AVCODEC;

  enum {
    SIZE_PATH=(sizeof path)/(sizeof path[0]),
    SIZE_AVCODEC=(sizeof AVCODEC)/(sizeof AVCODEC[0]),
  };

  if (avutil_load()<0)
    return -1;
  else if (swresample_load()<0)
    return -1;
  else if (!avcodec.node.hLib) {
    if ((path+SIZE_PATH)<=(pp+SIZE_AVCODEC)) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",AVCODEC);
#else // ] [
      _DMESSAGEV("loading %s",AVCODEC);
#endif // ]
      return -1;
    }

    FFSTRCPY(pp,AVCODEC);
    avcodec.node.hLib=FFDLOPEN(path,DLOPEN_FLAG);

    if (!avcodec.node.hLib&&!(avcodec.node.hLib=FFDLOPEN(AVCODEC,DLOPEN_FLAG))) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",path);
#else // ] [
      _DMESSAGEV("loading %s",path);
#endif // ]
      return -1;
    }

#if defined (FF_DYNLOAD_FREE_LIST) // [
    ff_dynload_list_append(&avcodec.node,FFL("avcodec"));
#endif // ]
  }

  return 0;
}

static int avformat_load(void)
{
  static const ffchar_t AVFORMAT[]=FF_AVFORMAT;

  enum {
    SIZE_PATH=(sizeof path)/(sizeof path[0]),
    SIZE_AVFORMAT=(sizeof AVFORMAT)/(sizeof AVFORMAT[0]),
  };

  if (avutil_load()<0)
    return -1;
  else if (avcodec_load()<0)
    return -1;
  else if (!avformat.node.hLib) {
    if ((path+SIZE_PATH)<=(pp+SIZE_AVFORMAT)) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",AVFORMAT);
#else // ] [
      _DMESSAGEV("loading %s",AVFORMAT);
#endif // ]
      return -1;
    }

    FFSTRCPY(pp,AVFORMAT);
    avformat.node.hLib=FFDLOPEN(path,DLOPEN_FLAG);

    if (!avformat.node.hLib&&!(avformat.node.hLib=FFDLOPEN(AVFORMAT,DLOPEN_FLAG))) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",path);
#else // ] [
      _DMESSAGEV("loading %s",path);
#endif // ]
      return -1;
    }

#if defined (FF_DYNLOAD_FREE_LIST) // [
    ff_dynload_list_append(&avformat.node,FFL("avformat"));
#endif // ]
  }

  return 0;
}

static int swresample_load(void)
{
  static const ffchar_t SWRESAMPLE[]=FF_SWRESAMPLE;

  enum {
    SIZE_PATH=(sizeof path)/(sizeof path[0]),
    SIZE_SWRESAMPLE=(sizeof SWRESAMPLE)/(sizeof SWRESAMPLE[0]),
  };

  if (avutil_load()<0)
    return -1;
  else if (!swresample.node.hLib) {
    if ((path+SIZE_PATH)<=(pp+SIZE_SWRESAMPLE)) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",SWRESAMPLE);
#else // ] [
      _DMESSAGEV("loading %s",SWRESAMPLE);
#endif // ]
      return -1;
   }

    FFSTRCPY(pp,SWRESAMPLE);
    swresample.node.hLib=FFDLOPEN(path,DLOPEN_FLAG);

    if (!swresample.node.hLib
        &&!(swresample.node.hLib=FFDLOPEN(SWRESAMPLE,DLOPEN_FLAG))) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",path);
#else // ] [
      _DMESSAGEV("loading %s",path);
#endif // ]
      return -1;
    }

#if defined (FF_DYNLOAD_FREE_LIST) // [
    ff_dynload_list_append(&swresample.node,FFL("swresample"));
#endif // ]
  }

  return 0;
}

static int swscale_load(void)
{
  static const ffchar_t SWSCALE[]=FF_SWSCALE;

  enum {
    SIZE_PATH=(sizeof path)/(sizeof path[0]),
    SIZE_SWSCALE=(sizeof SWSCALE)/(sizeof SWSCALE[0]),
  };

  if (avutil_load()<0)
    return -1;
  else if (!swscale.node.hLib) {
    if ((path+SIZE_PATH)<=(pp+SIZE_SWSCALE)) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",SWSCALE);
#else // ] [
      _DMESSAGEV("loading %s",SWSCALE);
#endif // ]
      return -1;
   }

    FFSTRCPY(pp,SWSCALE);
    swscale.node.hLib=FFDLOPEN(path,DLOPEN_FLAG);

    if (!swscale.node.hLib&&!(swscale.node.hLib=FFDLOPEN(SWSCALE,DLOPEN_FLAG))) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",path);
#else // ] [
      _DMESSAGEV("loading %s",path);
#endif // ]
      return -1;
    }

#if defined (FF_DYNLOAD_FREE_LIST) // [
    ff_dynload_list_append(&swscale.node,FFL("swscale"));
#endif // ]
  }

  return 0;
}

static int postproc_load(void)
{
  static const ffchar_t POSTPROC[]=FF_POSTPROC;

  enum {
    SIZE_PATH=(sizeof path)/(sizeof path[0]),
    SIZE_POSTPROC=(sizeof POSTPROC)/(sizeof POSTPROC[0]),
  };

  if (avutil_load()<0)
    return -1;
  else if (!postproc.node.hLib) {
    if ((path+SIZE_PATH)<=(pp+SIZE_POSTPROC)) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",POSTPROC);
#else // ] [
      _DMESSAGEV("loading %s",POSTPROC);
#endif // ]
      return -1;
    }

    FFSTRCPY(pp,POSTPROC);
    postproc.node.hLib=FFDLOPEN(path,DLOPEN_FLAG);

    if (!postproc.node.hLib&&!(postproc.node.hLib=FFDLOPEN(POSTPROC,DLOPEN_FLAG))) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",path);
#else // ] [
      _DMESSAGEV("loading %s",path);
#endif // ]
      return -1;
    }

#if defined (FF_DYNLOAD_FREE_LIST) // [
    ff_dynload_list_append(&postproc.node,FFL("postproc"));
#endif // ]
  }

  return 0;
}

static int avfilter_load(void)
{
  static const ffchar_t AVFILTER[]=FF_AVFILTER;

  enum {
    SIZE_PATH=(sizeof path)/(sizeof path[0]),
    SIZE_AVFILTER=(sizeof AVFILTER)/(sizeof AVFILTER[0]),
  };

  if (avcodec_load()<0)
    return -1;
  else if (avformat_load()<0)
    return -1;
  else if (avutil_load()<0)
    return -1;
  else if (postproc_load()<0)
    return -1;
  else if (swresample_load()<0)
    return -1;
  else if (swscale_load()<0)
    return -1;
  else if (!avfilter.node.hLib) {
    if ((path+SIZE_PATH)<=(pp+SIZE_AVFILTER)) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",AVFILTER);
#else // ] [
      _DMESSAGEV("loading %s",AVFILTER);
#endif // ]
      return -1;
    }

    FFSTRCPY(pp,AVFILTER);
    avfilter.node.hLib=FFDLOPEN(path,DLOPEN_FLAG);

    if (!avfilter.node.hLib&&!(avfilter.node.hLib=FFDLOPEN(AVFILTER,DLOPEN_FLAG))) {
#if defined (_WIN32) // [
      _DMESSAGEV("loading %S",path);
#else // ] [
      _DMESSAGEV("loading %s",path);
#endif // ]
      return -1;
    }

#if defined (FF_DYNLOAD_FREE_LIST) // [
    ff_dynload_list_append(&avfilter.node,FFL("avfilter"));
#endif // ]
  }

  return 0;
}

// ff_dynload.c ///////////////////////////////////////////////////////////////
static int avutil_load_sym(void *p, const char *sym)
{
  if (*(char **)p)
    return 0;
  else if (avutil_load()<0)
    return -1;
  else
    return load(avutil.node.hLib,sym,p);
}

static int avcodec_load_sym(void *p, const char *sym)
{
  if (*(char **)p)
    return 0;
  else if (avcodec_load()<0)
    return -1;
  else
    return load(avcodec.node.hLib,sym,p);
}

static int avformat_load_sym(void *p, const char *sym)
{
  if (*(char **)p)
    return 0;
  else if (avformat_load()<0)
    return -1;
  else
    return load(avformat.node.hLib,sym,p);
}

static int swresample_load_sym(void *p, const char *sym)
{
  if (*(char **)p)
    return 0;
  else if (swresample_load()<0)
    return -1;
  else
    return load(swresample.node.hLib,sym,p);
}

static int swscale_load_sym(void *p, const char *sym)
{
  if (*(char **)p)
    return 0;
  else if (swscale_load()<0)
    return -1;
  else
    return load(swscale.node.hLib,sym,p);
}

static int postproc_load_sym(void *p FFUNUSED, const char *sym FFUNUSED)
{
  if (*(char **)p)
    return 0;
  else if (postproc_load()<0)
    return -1;
  else
    return load(postproc.node.hLib,sym,p);
}

static int avfilter_load_sym(void *p FFUNUSED, const char *sym FFUNUSED)
{
  if (*(char **)p)
    return 0;
  else if (avfilter_load()<0)
    return -1;
  else
    return load(avfilter.node.hLib,sym,p);
}

///////////////////////////////////////////////////////////////////////////////
static int ff_dynload_absolute(const ffchar_t *dirname)
{
  enum { SIZE=(sizeof path)/(sizeof path[0]) };
  int code=-1;
  ffchar_t *mp=path+SIZE;
	int size=FFSTRLEN(dirname)+2;

  if (mp<=pp+size+1)
    goto exit;

	FFSTRCPY(path,dirname);
  pp+=size-1;
  *pp++=FFL('\\');
  *pp=FFL('\0');

  /////////////////////////////////////////////////////////////////////////////
  code=0;
exit:
  return code;
}

static int ff_dynload_relative(const ffchar_t *dirname)
{
  enum { SIZE=(sizeof path)/(sizeof path[0]) };
  int code=-1;
  ffchar_t *mp=path+SIZE;
#if defined (__linux__) // [
  char process_path[64];
#endif // ]
  int len;
  int size;

  /////////////////////////////////////////////////////////////////////////////
#if defined (_WIN32) // [
  len=GetModuleFileNameW(NULL,pp,mp-pp);

	if (ERROR_INSUFFICIENT_BUFFER==GetLastError())
    goto exit;
#elif defined (__APPLE__) // ] [
	proc_pidpath(getpid(),pp,path+(sizeof path)-pp);
	len=strlen(pp);
	//pp+=strlen(pp);
_DWRITELNV("\"%s\"",path);
#else // ] [
  sprintf(process_path,"/proc/%d/exe",getpid());

  if ((len=readlink(process_path,path,SIZE-1))<0)
    goto exit;

	len=strlen(pp);
  pp=path+len;
  *pp='\0';
#endif // ]

  if (mp<=pp+len+1)
    goto exit;

  pp+=len;

  /////////////////////////////////////////////////////////////////////////////
  while (path<pp&&FFL('/')!=pp[-1]&&FFL('\\')!=pp[-1])
    --pp;

	size=FFSTRLEN(dirname)+1;

  if (mp<=pp+size+1)
    goto exit;

	FFSTRCPY(pp,dirname);

  pp+=size-1;
#if defined (_WIN32) // [
  *pp++=FFL('\\');
#else // ] [
  *pp++=FFL('/');
#endif // ]
  *pp=FFL('\0');

  /////////////////////////////////////////////////////////////////////////////
#if defined (__APPLE__) // [
_DWRITELNV("\"%s\"",path);
#endif // ]
  code=0;
exit:
  return code;
}

void ff_unload(void);

const char *ff_dynload_path(void)
{
#if defined (_WIN32) // [
  static char patha[PATH_MAX];
  const char *lang=getenv("LANG");
  UINT uCodePage=!lang||!strstr(lang,"UTF-8")?CP_OEMCP:CP_UTF8;
#endif // ]

  *pp=FFL('\0');

#if defined (_WIN32) // [
  WideCharToMultiByte(
    uCodePage,        // UINT                               CodePage,
    0ul,              // DWORD                              dwFlags,
    path,             // _In_NLS_string_(cchWideChar)LPCWCH lpWideCharStr,
    -1,               // int                                cchWideChar,
    patha,            // LPSTR                              lpMultiByteStr,
    sizeof patha,     // int                                cbMultiByte,
    NULL,             // LPCCH                              lpDefaultChar,
    NULL              // LPBOOL                             lpUsedDefaultChar
  );

  return patha;
#else // ] [
  return path;
#endif // ]
}

int ff_dynload(const ffchar_t *dirname)
{
  int code=-1;

  if (NULL==dirname||'/'==dirname[0])
    code=ff_dynload_absolute(dirname);
  else if ('\\'==dirname[0]||(dirname[0]!=0&&dirname[1]==':'))
    code=ff_dynload_absolute(dirname);
  else
    code=ff_dynload_relative(dirname);

  if (code<0) {
#if defined (_WIN32) // [
    _DMESSAGEV("setting dirname \"%S\"",dirname);
#else // ] [
    _DMESSAGEV("setting dirname \"%s\"",dirname);
#endif // ]
    goto exit;
  }

  return 0;
//cleanup:
  ff_unload();
exit:
  return -1;
}

void ff_unload(void)
{
#if defined (FF_DYNLOAD_FREE_LIST) // [
  while (ff_dynload_tail) {
//FFVWRITELN("\"%s\"",ff_dynload_tail->id);
#if defined (_WIN32) // [
    FreeLibrary(ff_dynload_tail->hLib);
#else // ] [
    dlclose(ff_dynload_tail->hLib);
#endif // ]
    ff_dynload_tail=ff_dynload_tail->prev;
  }
#else // ] [
  if (avfilter.node.hLib) {
#if defined (_WIN32) // [
    FreeLibrary(avfilter.node.hLib);
#else // ] [
    dlclose(avfilter.node.hLib);
#endif // ]
	}

  if (postproc.node.hLib) {
#if defined (_WIN32) // [
    FreeLibrary(postproc.node.hLib);
#else // ] [
    dlclose(postproc.node.hLib);
#endif // ]
	}

  if (swscale.node.hLib) {
#if defined (_WIN32) // [
    FreeLibrary(swscale.node.hLib);
#else // ] [
    dlclose(swscale.node.hLib);
#endif // ]
	}

  if (avformat.node.hLib) {
#if defined (_WIN32) // [
    FreeLibrary(avformat.node.hLib);
#else // ] [
    dlclose(avformat.node.hLib);
#endif // ]
	}

  if (avcodec.node.hLib) {
#if defined (_WIN32) // [
    FreeLibrary(avcodec.node.hLib);
#else // ] [
    dlclose(avcodec.node.hLib);
#endif // ]
	}

  if (swresample.node.hLib) {
#if defined (_WIN32) // [
    FreeLibrary(swresample.node.hLib);
#else // ] [
    dlclose(swresample.node.hLib);
#endif // ]
	}

  if (avutil.node.hLib) {
#if defined (_WIN32) // [
    FreeLibrary(avutil.node.hLib);
#else // ] [
    dlclose(avutil.node.hLib);
#endif // ]
	}
#endif // ]
}
//#else // ] [
//#error 55
#endif // ]
