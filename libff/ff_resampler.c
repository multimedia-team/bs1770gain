/*
 * ff_resampler.c
 *
 * Copyright (C) 2019 Peter Belkner <info@pbelkner.de>
 * Nanos gigantum humeris insidentes #TeamWhite
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.0 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */
#include <ff.h>

#if 1 // {
#if defined (FF_BITMASK_BASED_CHANNEL_LAYOUT) // [
#undef FF_BITMASK_BASED_CHANNEL_LAYOUT
#endif // ]
#if defined (FF_HOLZHAMMER) // [
#undef FF_HOLZHAMMER
#endif // ]
#endif // }

#if defined (FF_BITMASK_BASED_CHANNEL_LAYOUT) // {
FF_DISABLE_DEPRECATION_WARNINGS // [
#endif // }

///////////////////////////////////////////////////////////////////////////////
int ff_resampler_create(ff_resampler_t *res,
    const AVCodecParameters *ocodecpar,
    const AVCodecParameters *icodecpar)
{
  int err=-1;
#if defined (FF_BITMASK_BASED_CHANNEL_LAYOUT) // [
  int64_t ochannel_layout=ocodecpar->channel_layout;

#endif // ]

#if defined (FF_RESAMPLER_RATE) // [
  /////////////////////////////////////////////////////////////////////////////
  res->irate=icodecpar->sample_rate;
  res->orate=ocodecpar->sample_rate;
#endif // ]
#if defined (FF_RESAMPLER_NB_SAMPLES) // [
  res->nb_samples=0;
#endif // ]

  /////////////////////////////////////////////////////////////////////////////
#if defined (FF_BITMASK_BASED_CHANNEL_LAYOUT) // [
  if (!ochannel_layout||!icodecpar->channel_layout) {
#if defined (_WIN32) // [
    fwprintf(stderr,L"invalid channel layout: output=%I64d input=%I64d",
        ochannel_layout,icodecpar->channel_layout);
#else // ] [
    fprintf(stderr,"invalid channel layout: output=%" PRId64 " input=%" PRId64,
        ochannel_layout,icodecpar->channel_layout);
#endif // ]
    goto e_channel_layout;
  }
#endif // ]

  /////////////////////////////////////////////////////////////////////////////
#if defined (FF_BITMASK_BASED_CHANNEL_LAYOUT) // [
  res->ctx=swr_alloc_set_opts(
      NULL,                       // struct SwrContext *s,
      // [
      ochannel_layout,            // int64_t out_ch_layout,
      ocodecpar->format,          // enum AVSampleFormat out_sample_fmt,
      ocodecpar->sample_rate,     // int out_sample_rate,
      // ] [
      icodecpar->channel_layout,  // int64_t in_ch_layout,
      icodecpar->format,          // enum AVSampleFormat in_sample_fmt,
      icodecpar->sample_rate,     // int in_sample_rate,
      // ]
      0,                          // int log_obgset,
      NULL);                      // void *log_ctx

  if (!res->ctx) {
    _DMESSAGE("allocating context");
    goto e_context;
  }
#else // ] [
  res->ctx=swr_alloc();

  if (!res->ctx) {
    _DMESSAGE("allocating context");
    goto e_context;
  }

  av_opt_set_chlayout(res->ctx, "in_chlayout",    &icodecpar->ch_layout, 0);
  av_opt_set_int(res->ctx, "in_sample_rate",       icodecpar->sample_rate, 0);
  av_opt_set_sample_fmt(res->ctx, "in_sample_fmt", icodecpar->format, 0);

  av_opt_set_chlayout(res->ctx, "out_chlayout",    &ocodecpar->ch_layout, 0);
  av_opt_set_int(res->ctx, "out_sample_rate",       ocodecpar->sample_rate, 0);
  av_opt_set_sample_fmt(res->ctx, "out_sample_fmt", ocodecpar->format, 0);
#endif // ]

  /////////////////////////////////////////////////////////////////////////////
  res->frame=av_frame_alloc();

  if (!res->frame) {
    _DMESSAGE("allocating frame");
    goto e_frame;
  }

#if defined (FF_BITMASK_BASED_CHANNEL_LAYOUT) // [
  res->frame->channel_layout=ochannel_layout;
  res->frame->channels=av_get_channel_layout_nb_channels(ochannel_layout);
#else // ] [
  res->frame->ch_layout=ocodecpar->ch_layout;
#endif // ]
  res->frame->format=ocodecpar->format;
  res->frame->sample_rate=ocodecpar->sample_rate;

  /////////////////////////////////////////////////////////////////////////////
  err=0;
//cleanup:
  av_frame_free(&res->frame);
e_frame:
  swr_close(res->ctx);
  swr_free(&res->ctx);
e_context:
#if defined (FF_BITMASK_BASED_CHANNEL_LAYOUT) // [
e_channel_layout:
#endif // ]
  return err;
}

void resampler_destroy(ff_resampler_t *res)
{
  av_frame_free(&res->frame);
  swr_close(res->ctx);
  swr_free(&res->ctx);
}

//#define FF_RESAMPLER_GET_BUFFER
int resampler_apply(ff_resampler_t *res, AVFrame *frame)
{
  int err;

#if defined (FF_RESAMPLER_RATE) // [
  /////////////////////////////////////////////////////////////////////////////
  if (frame) {
    int nb_samples=(uint64_t)res->orate/res->irate*frame->nb_samples;

    if (!res->frame->nb_samples) {
#if defined (FF_RESAMPLER_NB_SAMPLES) // [
      res->frame->nb_samples=res->nb_samples=nb_samples;
#else // ] [
      res->frame->nb_samples=nb_samples;
#endif // ]

#if defined (FF_RESAMPLER_GET_BUFFER) // [
      err=av_frame_get_buffer(res->frame,0);

      if (err<0) {
        _DMESSAGE("getting frame buffer");
        goto e_buffer;
      }
#endif // ]
    }
    else if (res->frame->nb_samples<nb_samples) {
#if defined (FF_RESAMPLER_NB_SAMPLES) // [
      if (res->nb_samples<nb_samples) {
#endif // ]
#if defined (FF_BITMASK_BASED_CHANNEL_LAYOUT) // [
        uint64_t channel_layout=res->frame->channel_layout;
        int channels=res->frame->channels;
#else // ] [
        AVChannelLayout ch_layout=res->frame->ch_layout;
        int nb_channels=res->frame->ch_layout.nb_channels;
#endif // ]
        int format=res->frame->format;
        int sample_rate=res->frame->sample_rate;

        av_frame_free(&res->frame);
        res->frame=av_frame_alloc();

        if (!res->frame) {
          _DMESSAGE("allocating frame");
          goto e_frame;
        }

#if defined (FF_RESAMPLER_NB_SAMPLES) // [
        res->frame->nb_samples=res->nb_samples=nb_samples;
#else // ] [
        //res->frame->nb_samples=res->nb_samples=nb_samples;
        res->frame->nb_samples=nb_samples;
#endif // ]
#if defined (FF_BITMASK_BASED_CHANNEL_LAYOUT) // [
        res->frame->channel_layout=channel_layout;
#else // ] [
        res->frame->ch_layout=ch_layout;
#endif // ]
#if defined (FF_BITMASK_BASED_CHANNEL_LAYOUT) // [
        res->frame->channels=channels;
#else // ] [
        res->frame->ch_layout.nb_channels=nb_channels;
#endif // ]
        res->frame->format=format;
        res->frame->sample_rate=sample_rate;

#if defined (FF_RESAMPLER_GET_BUFFER) // [
        err=av_frame_get_buffer(res->frame,0);

        if (err<0) {
          _DMESSAGE("getting frame buffer");
          goto e_buffer;
        }
#endif // ]
#if defined (FF_RESAMPLER_NB_SAMPLES) // [
      }
      else
        res->frame->nb_samples=nb_samples;
#endif // ]
    }
  }
#endif // ]

  /////////////////////////////////////////////////////////////////////////////
  err=swr_convert_frame(res->ctx,res->frame,frame);

  if (err<0) {
    _DMESSAGEV("converting frame: %s (%d)",av_err2str(err),err);
    goto econvert;
  }

  /////////////////////////////////////////////////////////////////////////////
  return 0;
econvert:
#if defined (FF_RESAMPLER_RATE) // [
#if defined (FF_RESAMPLER_GET_BUFFER) // [
e_buffer:
#endif // ]
e_frame:
#endif // ]
  return -1;
}

#if defined (FF_BITMASK_BASED_CHANNEL_LAYOUT)
FF_ENABLE_DEPRECATION_WARNINGS // ]
#endif
